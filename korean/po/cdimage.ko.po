# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Sangdo Jun <sebuls@gmail.com>, 2020
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-07-03 23:27+0900\n"
"Last-Translator: Sangdo Jun <sebuls@gmail.com>\n"
"Language-Team: debian-l10n-korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:13
msgid "      Key fingerprint"
msgstr "      Key fingerprint"

#: ../../english/devel/debian-installer/images.data:87
msgid "ISO images"
msgstr "ISO 이미지"

#: ../../english/devel/debian-installer/images.data:88
msgid "Jigdo files"
msgstr ""

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr ""

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />FAQ"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "jigdo를 이용한 다운로드"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "http/ftp를 통한 다운로드"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "CD 또는 DVD 구입"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "네트워크 설치"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />다운로드"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />기타"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id=\"dc_artwork\" />아트워크"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />미러링"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Rsync 미러"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_verify\" />검증"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />토런트로 다운로드"

#: ../../english/template/debian/cdimage.wml:49
msgid "Debian CD team"
msgstr "데비안 CD 팀"

#: ../../english/template/debian/cdimage.wml:52
msgid "debian_on_cd"
msgstr ""

#: ../../english/template/debian/cdimage.wml:55
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />faq"

#: ../../english/template/debian/cdimage.wml:58
msgid "jigdo"
msgstr ""

#: ../../english/template/debian/cdimage.wml:61
msgid "http_ftp"
msgstr ""

#: ../../english/template/debian/cdimage.wml:64
msgid "buy"
msgstr "구입"

#: ../../english/template/debian/cdimage.wml:67
msgid "net_install"
msgstr ""

#: ../../english/template/debian/cdimage.wml:70
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />기타"

#: ../../english/template/debian/cdimage.wml:73
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />이미지 릴리스 정보"
