#use wml::debian::template title="데비안 소개" MAINPAGE="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c" maintainer="Seunghun Han (kkamagui)"

<a id=community></a>
<h2>데비안은 사람들의 커뮤니티입니다.</h2>
<p>전 세계 수천 명의 자원봉사자가 자유 소프트웨어와 사용자 요구사항을 우선적으로 생각하며 함께 일합니다.</p>

<ul>
  <li>
    <a href="people">사람들:</a>
    우리는 누구이며, 무슨 일을 하는가
  </li>
  <li>
    <a href="philosophy">철학:</a>
    우리는 왜 데비안을 하며, 어떻게 하는가
  </li>
  <li>
    <a href="../devel/join/">참여:</a>
    여러분도 커뮤니티 일원이 될 수 있습니다!
  </li>
  <li>
    <a href="help">어떻게 데비안을 도울 수 있는가</a>
  </li>
  <li>
    <a href="../social_contract">우리의 약속:</a>
    우리의 도덕적 의무
  </li>
  <li>
    <a href="diversity">다양성 선언</a>
  </li>
  <li>
    <a href="../code_of_conduct">행동 강령</a>
  </li>
  <li> <a href="../partners/">파트너:</a> 데비안 프로젝트를 지속적으로 지원하는 회사와 기업 </li>
  <li>
    <a href="../donations">기부</a>
  </li>
  <li>
    <a href="../legal/">법률 정보 </a>
  </li>
  <li>
    <a href="../legal/privacy">개인 정보</a>
  </li>
  <li>
    <a href="../contact">연락처</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>데비안은 자유 운영체제입니다.</h2>
<p>우리는 리눅스와 함께 시작했으며, 데비안 사용자의 요구사항을 만족시킬 수 있는 수천 개의 응용프로그램을 추가했습니다.</p>

<ul>
  <li>
    <a href="../distrib">다운로드:</a>
    보다 다양한 데비안 이미지
  </li>
  <li>
  <a href="why_debian">왜 데비안인가</a>
  </li>
  <li>
    <a href="../support">지원:</a>
    도움 받기
  </li>
  <li>
    <a href="../security">보안:</a>
    최신 업데이트 <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages"> 소프트웨어 패키지:</a>
    긴 소프트웨어 목록을 찾고 살펴보기
  </li>
  <li>
    <a href="../doc"> 문서</a>
  </li>
  <li>
    <a href="https://wiki.debian.org"> 데비안 위키</a>
  </li>
  <li>
    <a href="../Bugs"> 버그 리포트</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    메일링 리스트</a>
  </li>
  <li>
    <a href="../blends"> 퓨어 블렌드:</a>
    특별한 요구사항을 위한 메타 패키지
  </li>
  <li>
    <a href="../devel"> 개발자 코너:</a>
    데비안 개발자에게 흥미로운 정보
  </li>
  <li>
    <a href="../ports"> 포트/아키텍처:</a>
    지원하는 CPU 아키텍처
  </li>
  <li>
    <a href="search">데비안 검색 엔진을 사용하는 방법에 대한 정보</a>.
  </li>
  <li>
    <a href="cn">다양한 언어로 된 페이지에 대한 정보</a>.
  </li>
</ul>

