#use wml::debian::ddp title="Podręczniki deweloperów DDP"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"
#use wml::debian::translation-check translation="572445c4e984548ff493fb061c947af8f4e4abc2"

<document "Podręcznik Polityki Debiana" "policy">

<div class="centerblock">
<p>
  Podręcznik przedstawia zasady dotyczące dystrybucji Debian GNU/Linux.
  Dotyczą one struktury i zawartości archiwum Debiana,
  kwesti projektowania systemu operacyjnego oraz wymogów
  technicznych, które musi spełnić każdy pakiet, aby został włączony do
  dystrybucji.

<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "Grupa Polityki Debiana">
  <status>
  skończone
  </status>
  <availability>
  <inpackage "debian-policy">
  <inddpvcs-debian-policy>
  <p><a href="https://bugs.debian.org/debian-policy">Proponowane poprawki</a>
  do Polityki

  <p>Dodatkowa dokumentacja dotycząca Polityki:</p>
  <ul>
    <li><a href="packaging-manuals/fhs/fhs-3.0.html">Filesystem Hierarchy Standard</a>
    [<a href="packaging-manuals/fhs/fhs-3.0.pdf">PDF</a>]
    [<a href="packaging-manuals/fhs/fhs-3.0.txt">zwykły tekst</a>]
    <li><a href="debian-policy/upgrading-checklist.html">Upgrading checklist</a>
    <li><a href="packaging-manuals/virtual-package-names-list.yaml">Lista nazw pakietów wirtualnych</a>
    <li><a href="packaging-manuals/menu-policy/">Polityka dotycząca Menu</a>
    [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">zwykły tekst</a>]
    <li><a href="packaging-manuals/perl-policy/">Polityka dotycząca Perla</a>
    [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">zwykły tekst</a>]
    <li><a href="packaging-manuals/debconf_specification.html">Specyfikacje debconf</a>
    <li><a href="packaging-manuals/debian-emacs-policy">Polityka dotycząca Emacsen</a>
    <li><a href="packaging-manuals/java-policy/">Polityka dotycząca Javy</a>
    <li><a href="packaging-manuals/python-policy/">Polityka dotycząca Pythona</a>
    <li><a href="packaging-manuals/copyright-format/1.0/">specyfikacja formatu pliku copyright</a>
  </ul>
  </availability>
</doctable>
</div>

<hr>

<document "Podręcznik dewelopera Debiana" "devref">

<div class="centerblock">
<p>
  Podręcznik przedstawia procedury i zasoby przeznaczone dla deweloperów
  Debiana. Opisuje jak zostać nowym deweloperem, procedurę przesyłania
  pakietów do archiwum, jak posługiwać
  się naszym systemem śledzenia błędów, listami dyskusyjnymi, serwerami
  internetowymi itp.

  <p>Jest uważany jako <em>podstawowy podręcznik</em> dla wszystkich
  deweloperów Debiana (nowych i bardziej doświadczonych).

<doctable>
  <authors "Ian Jackson, Christian Schwarz, Adam Di Carlo, Rapha&euml;l Hertzog, Josip Rodin">
  <maintainer "Adam Di Carlo, Rapha&euml;l Hertzog, Josip Rodin">
  <status>
  skończone
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpvcs-developers-reference>
  </availability>
</doctable>
</div>

<hr>

<document "Podręcznik dla nowych Opiekunów Debiana" "maint-guide">

<div class="centerblock">
<p>
  Dokument prostym językiem opisuje sposób budowania pakietów dla
  Debian GNU/Linux. Jest przeznaczony zarówno dla zwykłych użytkowników
  Debiana jak i osób chcących zostać deweloperami. Zawiera wiele
  praktycznych przykładów.

  <p>W przeciwieństwie do poprzednich dokumentów tego typu bazuje on na
  <code>debhelperze</code> oraz na nowych narzędziach dostępnych dla
  deweloperów. Głównym zamierzeniem autora jest ujednolicenie i połączenie
  poprzednich projektów.

<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  skończony
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpvcs-maint-guide>
  </availability>
</doctable>
</div>

<hr>

<document "Wprowadzenie do pracy z pakietami w Debianie" "packaging-tutorial">

<div class="centerblock">
<p>
Podręcznik jest wprowadzeniem do pracy z pakietami w Debianie.
Uczy przyszłych deweloperów jak modyfikować istniejące pakiety, tworzyć
własne pakiety oraz jak współpracować ze społecznością Debiana.
Dodatkowo zawiera trzy praktyczne przykłady: modyfikacja
pakietu <code>grep</code>, tworzenie pakietu dla gry <code>gnujump</code>
oraz biblioteki Javy.
</p>

<doctable>
  <authors "Lucas Nussbaum">
  <maintainer "Lucas Nussbaum">
  <status>
  skończony
  </status>
  <availability>
  <inpackage "packaging-tutorial">
  <inddpvcs-packaging-tutorial>
  </availability>
</doctable>
</div>

<hr>

<document "System Menu Debiana" "menu">

<div class="centerblock">
<p>
  Podręcznik ten przedstawia System Menu Debiana oraz pakiet
  <strong>menu</strong>.

  <p>Pakiet menu wywodzi się z programu install-fvwm2-menu zawartego w
  pakiecie starego fvwm2. Jednakże, założeniem menu jest stworzenie ogólnego
  interfejsu dla tworzenia menu. Dzięki poleceniu update-menus pochodzącemu z
  tego pakietu żaden pakiet nie musi zostać ponownie modyfikowany dla każdego
  kolejnego menedżera X window, ponadto dostarcza ujednolicony interfejs
  zarówno dla tekstowych jak i X-owych programów.

<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  skończony
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">wersja online HTML</a>
  </availability>
</doctable>
</div>

<hr>

<document "Instalator Debiana od środka" "d-i-internals">

<div class="centerblock">
<p>
  Celem dokumentu jest przybliżenie nowym deweloperom Instalatora Debiana
  oraz zebranie dokumentacji technicznej w jednym miejscu.

<doctable>
  <authors "Frans Pop">
  <maintainer "Debian Installer team">
  <status>
  skończony
  </status>
  <availability>
  <p><a href="https://d-i.alioth.debian.org/doc/internals/">HTML online</a>.</p>
  <p><a href="https://anonscm.debian.org/cgit/d-i/debian-installer.git/tree/doc/devel/internals">DocBook XML source online</a>.</p>
  </availability>
</doctable>
</div>

