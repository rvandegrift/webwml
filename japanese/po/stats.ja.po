msgid ""
msgstr ""
"Project-Id-Version: stats.po\n"
"PO-Revision-Date: 2011-03-20 06:09+0900\n"
"Last-Translator: Osamu Aoki <osamu@debian.org>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Debian ウェブサイト翻訳状況"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "翻訳するページ数 %d"

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "翻訳するバイト数 %d"

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "翻訳する文の数 %d"

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "翻訳のバージョン番号が変です"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "翻訳が古すぎです"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "原版がこの翻訳より新しくなっています"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "原版がなくなっています"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "ヒットカウント無し"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "ヒット"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "クリックすると diffstat データを取得します"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "翻訳状況:"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "未翻訳"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "未更新"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "翻訳済"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "最新"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "ファイル"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "バイト"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"注: このページのリストは人気順に並んでいます。ページ名にマウスカーソルを持っ"
"て行くとヒット数を確認できます。"

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "古くなっている翻訳"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "ファイル"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "差分"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "状態"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:644
msgid "Log"
msgstr "ログ"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "翻訳版"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "担当者"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "状態"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "翻訳者"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "日時"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "未翻訳の一般ページ"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "未翻訳の一般ページ"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "未翻訳のニュース項目"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "未翻訳のニュース項目"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "未翻訳のコンサルタント/ユーザページ"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "未翻訳のコンサルタント/ユーザページ"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "未翻訳の国際化ページ"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "未翻訳の国際化ページ"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "翻訳済ページ (最新)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "翻訳済テンプレート (PO ファイル)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "PO 翻訳状況"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Fuzzy"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "未翻訳"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "合計"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "合計:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "翻訳済ページ"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "ページ単位で集計した翻訳状況"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "言語"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "翻訳版"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "翻訳済ページ (サイズ)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "ページサイズから見た翻訳状況"

#~ msgid "Created with"
#~ msgstr "Created with"

#~ msgid "Origin"
#~ msgstr "原版"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "差分(色分け)"

#~ msgid "Colored diff"
#~ msgstr "差分(色分け)"

#~ msgid "Unified diff"
#~ msgstr "差分(統合)"
