#use wml::debian::template title="Situación de los desarrolladores"
#use wml::debian::translation-check translation="6c996020eca890cac2a0243d4633d9bfa7935674"

<p>Muchas personas han indicado su interés en tener información
sobre la localización de los desarrolladores de Debian.
Así pues, hemos decidido añadir, como parte de la base de datos
de desarrolladores, un campo donde los desarrolladores pueden
especificar sus coordenadas geográficas.

<p>El mapa mostrado más abajo está generado a partir de una
<a href="developers.coords">lista de coordenadas de desarrolladores</A>
(de la que han sido eliminados los nombres)
utilizando el programa
<a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a>.

<p><img src="developers.map.jpeg" alt="Mapa del mundo">

<p>Si Vd. es un desarrollador y desea añadir sus coordenadas
a su entrada de la base de datos, entre en la
<a href="https://db.debian.org">base de datos de desarrolladores de Debian</a>
y modifique su entrada. Si no conoce las coordenadas de su ciudad,
debería ser capaz de encontrarla en alguno de los siguientes servidores:
<ul>
<li><a href="https://osm.org">Openstreetmap</a>
Busque su ciudad en la barra de búsqueda.
Pulse en el icono que muestra unas flechas junto a la barra de búsqueda. Después, arrastre el
marcador verde al mapa OSM. Las coordenadas se mostrarán en el campo «From».
</ul>

<p>El formato para las coordenadas es uno de los siguientes:
<dl> 
<dt>Grados decimales
<dd>El formato es +-GGG.GGGGGGGGGGGGGGG. Este es el formato que
    programas como xearth usan y el formato que muchos webs de
    posicionamiento usan. Sin embargo, típicamente la precisión está
    limitada a 4 ó 5 decimales.
<dt>Grados Minutos (DGM) 
<dd>El formato es +-GGGMM.MMMMMMMMMMMMM. No es un tipo aritmético,
    sino una representación de dos unidades separadas, grados y
    minutos. Esta es la salida típica de muchas unidades GPS de mano
    y de los mensajes GPS en format NMEA.
<dt>Grados Minutos Segundos (DGMS) 
<dd>El formato es +-GGGMMSS.SSSSSSSSSSS. Como en DGM, no es un tipo
    aritmético sino una representación empaquetada de tres unidades
    separadas: grados, minutos y segundos. Esta es la salida típica de
    sitios web que devuelven tres valores para cada posición. Por
    ejemplo, 34:50:12.24523 Norte puede ser la posición devuelta, que
    en DGMS sería +0345012.24523.
</dl> 
<p> 
Para latitud, + es «norte», y para la longitud, + es «este». Es importante
especificar suficientes ceros para no hacer ambiguo el formato que se
use si su posición está a menos de 2 grados de un punto cero. 
