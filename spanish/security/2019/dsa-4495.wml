#use wml::debian::translation-check translation="2b74abc752f8e4232fe85da5b7c01782113a2f4d"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el núcleo Linux que
pueden dar lugar a elevación de privilegios, a denegación de servicio o a fugas
de información.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20836">CVE-2018-20836</a>

    <p>chenxiang informó de una condición de carrera en libsas, el subsistema
    del núcleo para soporte de dispositivos SCSI en serie (SAS, por sus siglas en inglés), que
    podría dar lugar a un «uso tras liberar». No está claro cómo podría
    explotarse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1125">CVE-2019-1125</a>

    <p>Se descubrió que la mayoría de los procesadores x86 podrían saltarse,
    de forma especulativa, una instrucción SWAPGS condicional utilizada al entrar al
    núcleo desde modo usuario, o que podrían ejecutarla, de forma especulativa, cuando
    deberían saltársela. Este es un subtipo de Spectre variante 1
    que podría permitir que usuarios locales obtengan información sensible
    del núcleo o de otros procesos. Se ha mitigado utilizando
    barreras de memoria para limitar la ejecución especulativa. A los sistemas con
    núcleo i386 no les afecta ya que el núcleo no utiliza SWAPGS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1999">CVE-2019-1999</a>

    <p>Se descubrió una condición de carrera en el controlador de Android binder
    que podría dar lugar a un «uso tras liberar». Si está cargado este controlador, un
    usuario local podría aprovecharse de esto para provocar una denegación de servicio
    (corrupción de memoria) o para elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10207">CVE-2019-10207</a>

    <p>La herramienta syzkaller encontró una potencial desreferencia nula en varios
    controladores para adaptadores de Bluetooth en serie (conectados a UART). Un usuario local con
    acceso a un dispositivo pty o a otro dispositivo tty adecuado podría aprovecharse de esto
    para provocar una denegación de servicio (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10638">CVE-2019-10638</a>

    <p>Amit Klein y Benny Pinkas descubrieron que la generación de ID
    de paquetes IP utilizaba una función hash débil: <q>jhash</q>. Esto podría permitir
    el seguimiento de ordenadores individuales al comunicarse con distintos
    servidores remotos y desde redes también distintas. En su lugar,
    ahora se utiliza la función <q>siphash</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12817">CVE-2019-12817</a>

    <p>Se descubrió que en la arquitectura PowerPC (ppc64el) el
    código de la tabla hash de páginas (HPT, por sus siglas en inglés) no gestionaba correctamente los fork() en
    procesos con memoria ubicada en direcciones por encima de 512 TiB. Esto podría
    dar lugar a «uso tras liberar» en el núcleo o a la compartición no intencionada de
    memoria entre procesos de usuario. Un usuario local podría aprovecharse de esto para
    elevación de privilegios. Los sistemas que utilizan el radix MMU o un núcleo
    personalizado con tamaño de página de 4 KiB no están afectados.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12984">CVE-2019-12984</a>

    <p>Se descubrió que la implementación del protocolo NFC no
    validaba correctamente un mensaje de control de netlink, dado lugar, potencialmente,
    a una desreferencia de puntero nulo. Un usuario local en un sistema con una
    interfaz NFC podría usar esto para provocar denegación de servicio (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13233">CVE-2019-13233</a>

    <p>Jann Horn descubrió una condición de carrera en la arquitectura x86,
    en el uso de la LDT. Esto podría dar lugar a un «uso tras liberar». Un
    usuario local podría, posiblemente, utilizar esto para provocar denegación de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13631">CVE-2019-13631</a>

    <p>Se descubrió que el controlador gtco para entrada USB en tabletas podría
    desbordar la pila con datos constantes durante el análisis sintáctico del descriptor del
    dispositivo. Un usuario presente físicamente con un dispositivo
    USB construido de una manera especial podría usar esto para provocar denegación de servicio
    (BUG/oops) o, posiblemente, para elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13648">CVE-2019-13648</a>

    <p>Praveen Pandey informó de que en sistemas PowerPC (ppc64el) sin
    memoria transaccional (TM, por sus siglas en inglés), el núcleo, de todas formas, intentaría
    restaurar el estado de TM pasado a la llamada al sistema sigreturn(). Un usuario
    local podría usar esto para provocar denegación de servicio (oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14283">CVE-2019-14283</a>

    <p>La herramienta syzkaller encontró que faltaba una comprobación de límites en el controlador de
    disquete. Un usuario local con acceso a una disquetera con un
    disquete en su interior podría usar esto para leer memoria del núcleo fuera de los límites del
    área de E/S y obtener, posiblemente, información sensible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14284">CVE-2019-14284</a>

    <p>La herramienta syzkaller encontró una potencial división por cero en el
    controlador de disquete. Un usuario local con acceso a una
    disquetera podría usar esto para provocar denegación de servicio (oops).</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 4.19.37-5+deb10u2.</p>

<p>Para la distribución «antigua estable» (stretch), estos problemas se corregirán
próximamente.</p>

<p>Le recomendamos que actualice los paquetes de linux.</p>

<p>Para información detallada sobre el estado de seguridad de linux, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4495.data"
