#use wml::debian::translation-check translation="6bde107f0f532a81d16326f47a68480ab5eb7957"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han encontrado múltiples problemas de seguridad en el navegador web Mozilla
Firefox que podrían, potencialmente, dar lugar a ejecución de código
arbitrario, a ejecución de scripts entre sitios («cross-site scripting»), a suplantación («spoofing»), a revelación de información, a denegación de
servicio o a falsificación de peticiones entre sitios («cross-site request forgery»).</p>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 60.8.0esr-1~deb9u1.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 60.8.0esr-1~deb10u1.</p>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2019-11719">\
CVE-2019-11719</a> y
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11729">\
CVE-2019-11729</a> se abordan solo para stretch, en buster Firefox utiliza
la copia global para el sistema de NSS, que se actualizará de forma independiente.</p>

<p>Le recomendamos que actualice los paquetes de firefox-esr.</p>

<p>Para información detallada sobre el estado de seguridad de firefox-esr, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4479.data"
