#use wml::debian::translation-check translation="669c87408de3af72c047aaa7ef3786903984b7ba"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron varios problemas de seguridad en QEMU, un emulador de procesadores
rápido:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12829">CVE-2020-12829</a>

    <p>Un desbordamiento de entero en el dispositivo de pantalla sm501 puede dar lugar a denegación de
    servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14364">CVE-2020-14364</a>

    <p>Una escritura fuera de límites en el código de emulación de USB puede dar lugar a
    ejecución de código del huésped al anfitrión («guest-to-host»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15863">CVE-2020-15863</a>

    <p>Un desbordamiento de memoria en el dispositivo de red XGMAC puede dar lugar a denegación de
    servicio o a ejecución de código arbitrario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16092">CVE-2020-16092</a>

    <p>Una aserción susceptible de ser desencadenada en los dispositivos e1000e y vmxnet3 puede dar lugar a
    denegación de servicio.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 1:3.1+dfsg-8+deb10u8.</p>

<p>Le recomendamos que actualice los paquetes de qemu.</p>

<p>Para información detallada sobre el estado de seguridad de qemu, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4760.data"
