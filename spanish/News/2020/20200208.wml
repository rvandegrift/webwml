#use wml::debian::translation-check translation="96c05360e385187167f1ccbce37d38ce2e5e6920"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.3</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la tercera actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction alot "Elimina fecha de expiración de las claves del juego de pruebas, corrigiendo fallo de compilación">
<correction atril "Corrige violación de acceso cuando no se carga ningún documento; corrige lectura de memoria no inicializada [CVE-2019-11459]">
<correction base-files "Actualizado para esta versión">
<correction beagle "Proporciona un script «wrapper» en lugar de enlaces simbólicos a los JAR, haciendo que funcionen de nuevo">
<correction bgpdump "Corrige violación de acceso">
<correction boost1.67 "Corrige comportamiento indefinido que da lugar a la caída de libboost-numpy">
<correction brightd "Compara realmente el valor leído de /sys/class/power_supply/AC/online con <q>0</q>">
<correction casacore-data-jplde "Incluye tablas hasta 2040">
<correction clamav "Nueva versión del proyecto original; corrige problema de denegación de servicio [CVE-2019-15961]; elimina la opción ScanOnAccess, la sustituye por clamonacc">
<correction compactheader "Nueva versión del proyecto original compatible con Thunderbird 68">
<correction console-common "Corrige regresión que hacía que no se incluyeran ficheros">
<correction csh "Corrige violación de acceso en eval">
<correction cups "Corrige filtración de contenido de la memoria en ppdOpen; corrige validación del idioma por omisión en ippSetValuetag [CVE-2019-2228]">
<correction cyrus-imapd "Añade tipo BACKUP a cyrus-upgrade-db, corrigiendo problemas al actualizar">
<correction debian-edu-config "Conserva la configuración de proxy en el cliente si no se puede conectar con el WPAD">
<correction debian-installer "Recompilado contra proposed-updates; modificada la generación de mini.iso en arm de forma que funcione el arranque desde red EFI; actualizado el valor de USE_UDEBS_FROM por omisión, de unstable a buster, para ayudar a los usuarios a realizar compilaciones locales">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction debian-security-support "Actualizado el estado del soporte de seguridad de varios paquetes">
<correction debos "Recompilado contra golang-github-go-debos-fakemachine actualizado">
<correction dispmua "Nueva versión del proyecto original compatible con Thunderbird 68">
<correction dkimpy "Nueva versión «estable» del proyecto original">
<correction dkimpy-milter "Corrige la gestión de privilegios en el arranque de forma que funcionen los sockets Unix">
<correction dpdk "Nueva versión «estable» del proyecto original">
<correction e2fsprogs "Corrige subdesbordamiento de pila potencial en e2fsck [CVE-2019-5188]; corrige «uso tras liberar» en e2fsck">
<correction fig2dev "Permite que las cadenas de texto Fig v2 terminen con múltiples ^A [CVE-2019-19555]; rechaza tipos de flecha enormes que provocan desbordamiento de entero [CVE-2019-19746]; corrige varias caídas [CVE-2019-19797]">
<correction freerdp2 "Corrige el tratamiento del código de retorno devuelto por realloc [CVE-2019-17177]">
<correction freetds "tds: se asegura de que UDT tiene varint configurado a 8 [CVE-2019-13508]">
<correction git-lfs "Corrige problemas de compilación con versiones de Go más recientes">
<correction gnubg "Incrementa el tamaño de las zonas de memoria estáticas utilizadas para componer mensajes durante el arranque del programa para que la traducción a español no provoque desbordamiento de memoria">
<correction gnutls28 "Corrige problemas de interoperabilidad con gnutls 2.x; corrige el análisis sintáctico de certificados que contienen RegisteredID">
<correction gtk2-engines-murrine "Corrige la posibilidad de instalación conjuntamente con otros temas">
<correction guile-2.2 "Corrige error de compilación">
<correction libburn "Corrige <q>la grabación multipista con cdrskin era lenta y se detenía tras la pista 1</q>">
<correction libcgns "Corrige error de compilación en ppc64el">
<correction libimobiledevice "Gestiona correctamente escrituras SSL parciales">
<correction libmatroska "Incrementa la dependencia de la biblioteca compartida a 1.4.7 porque esa versión introdujo nuevos símbolos">
<correction libmysofa "Correcciones de seguridad [CVE-2019-16091 CVE-2019-16092 CVE-2019-16093 CVE-2019-16094 CVE-2019-16095]">
<correction libole-storage-lite-perl "Corrige interpretación de años del 2020 en adelante">
<correction libparse-win32registry-perl "Corrige interpretación de años del 2020 en adelante">
<correction libperl4-corelibs-perl "Corrige interpretación de años del 2020 en adelante">
<correction libsolv "Corrige desbordamiento de memoria dinámica («heap») [CVE-2019-20387]">
<correction libspreadsheet-wright-perl "Corrige hojas de cálculo OpenDocument que antes no eran utilizables y el paso de opciones de formateo JSON">
<correction libtimedate-perl "Corrige interpretación de años del 2020 en adelante">
<correction libvirt "Apparmor: permite ejecutar pygrub; no incluye osxsave, ospke en la línea de órdenes de QEMU; esto ayuda a QEMU más recientes con algunas configuraciones generadas por virt-install">
<correction libvncserver "RFBserver: no filtra contenido de la pila al remoto [CVE-2019-15681]; resuelve una congelación («freeze») durante el cierre de conexión y una violación de acceso en servidores VNC multihilo; corrige problema al conectarse a servidores de VMWare; corrige caída de x11vnc cuando se conecta vncviewer">
<correction limnoria "Corrige revelación de información remota y, posiblemente, ejecución de código remoto en la extensión («plugin») Math [CVE-2019-19010]">
<correction linux "Nueva versión «estable» del proyecto original">
<correction linux-latest "Actualizado para la ABI del núcleo 4.19.0-8">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original">
<correction mariadb-10.3 "Nueva versión «estable» del proyecto original [CVE-2019-2938 CVE-2019-2974 CVE-2020-2574]">
<correction mesa "Llama a shmget() con permisos 0600, en lugar de 0777 [CVE-2019-5068]">
<correction mnemosyne "Añade dependencia con PIL, que faltaba">
<correction modsecurity "Corrige fallo en el análisis sintáctico de las cabeceras de cookies [CVE-2019-19886]">
<correction node-handlebars "Impide las llamadas directas a <q>helperMissing</q> y a <q>blockHelperMissing</q> [CVE-2019-19919]">
<correction node-kind-of "Corrige vulnerabilidad de comprobación de tipos en ctorName() [CVE-2019-20149]">
<correction ntpsec "Corrige reintentos de DNS lentos; modifica ntpdate -s (syslog) para corregir el «hook» if-up; correcciones de la documentación">
<correction numix-gtk-theme "Corrige la posibilidad de instalación conjuntamente con otros temas">
<correction nvidia-graphics-drivers-legacy-340xx "Nueva versión «estable» del proyecto original">
<correction nyancat "Recompilado en un entorno limpio para añadir la unidad de systemd para nyancat-server">
<correction openjpeg2 "Corrige desbordamiento de memoria dinámica («heap») [CVE-2018-21010] y desbordamiento de entero [CVE-2018-20847]">
<correction opensmtpd "Avisa a los usuarios del cambio en la sintaxis de smtpd.conf (en versiones anteriores); instala smtpctl setgid opensmtpq; gestiona códigos de retorno de hostname distintos de cero en la fase de configuración">
<correction openssh "Rechaza (de forma no fatal) ipc en el entorno aislado («sandbox») seccomp, corrigiendo errores con OpenSSL 1.1.1d y Linux &lt; 3.19 en algunas arquitecturas">
<correction php-horde "Corrige problema de ejecución directa de scrips entre sitios («stored cross-site scripting») en Horde Cloud Block [CVE-2019-12095]">
<correction php-horde-text-filter "Corrige expresiones regulares inválidas">
<correction postfix "Nueva versión «estable» del proyecto original">
<correction postgresql-11 "Nueva versión «estable» del proyecto original">
<correction print-manager "Corrige caída si CUPS devuelve el mismo ID para varios trabajos de impresión">
<correction proftpd-dfsg "Corrige problemas de CRL [CVE-2019-19270 CVE-2019-19269]">
<correction pykaraoke "Corrige ruta de tipos de letra">
<correction python-evtx "Corrige importación de <q>hexdump</q>">
<correction python-internetarchive "Cierra fichero tras obtener el hash, evitando que se agoten los descriptores de ficheros">
<correction python3.7 "Correcciones de seguridad [CVE-2019-9740 CVE-2019-9947 CVE-2019-9948 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935]">
<correction qtbase-opensource-src "Añade soporte para impresoras no-PPD y evita el uso, sin notificación, de una impresora PPD; corrige caída al utilizar QLabels con texto enriquecido; corrige eventos de tableta gráfica">
<correction qtwebengine-opensource-src "Corrige análisis sintáctico de PDF; inhabilita la pila ejecutable">
<correction quassel "Corrige denegaciones de AppArmor en quasselcore cuando se graba la configuración; canal por omisión correcto para Debian; elimina fichero NEWS innecesario">
<correction qwinff "Corrige caída debida a detección incorrecta de ficheros">
<correction raspi3-firmware "Corrige detección de consola serie con núcleos 5.x">
<correction ros-ros-comm "Corrige problemas de seguridad [CVE-2019-13566 CVE-2019-13465 CVE-2019-13445]">
<correction roundcube "Nueva versión «estable» del proyecto original; corrige permisos inseguros en la extensión («plugin») enigma [CVE-2018-1000071]">
<correction schleuder "Corrige el reconocimiento de palabras clave en correos electrónicos con <q>protected headers</q> y sin «asunto»; elimina firmas de terceros («non-self-signatures») al refrescar o descargar claves; error si el argumento proporcionado a «refresh_keys» no es una lista existente; añade cabecera List-Id, que faltaba, a los correos electrónicos de notificación enviados a los administradores; gestiona problemas de descifrado de forma más elegante; codificación ASCII-8BIT por omisión">
<correction simplesamlphp "Corrige incompatibilidad con PHP 7.3">
<correction sogo-connector "Nueva versión del proyecto original compatible con Thunderbird 68">
<correction spf-engine "Corrige la gestión de privilegios en el arranque de forma que funcionen los sockets Unix; actualiza la documentación de TestOnly">
<correction sudo "Corrige un desbordamiento de memoria (no explotable en buster) cuando está habilitado pwfeedback y la entrada no es un tty [CVE-2019-18634]">
<correction systemd "Da a fs.file-max sysctl el valor LONG_MAX en lugar de ULONG_MAX; cambia propietario y permisos de los directorios de ejecución también para los usuarios estáticos, asegurándose de que a los directorios de ejecución como CacheDirectory y StateDirectory se les asigna correctamente como propietario el usuario especificado en User= antes de lanzar el servicio">
<correction tifffile "Corrige script «wrapper»">
<correction tigervnc "Correcciones de seguridad [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Correcciones de seguridad [CVE-2014-6053 CVE-2019-8287 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction uif "Corrige las rutas a ip(6)tables-restore teniendo en cuenta la migración a nftables">
<correction unhide "Corrige agotamiento de la pila">
<correction x2goclient "Elimina ~/, ~user{,/}, ${HOME}{,/} y $HOME{,/} de las rutas de destino en modo SCP; corrige regresión con versiones más recientes de libssh que tengan aplicadas las correcciones para CVE-2019-14889">
<correction xmltooling "Corrige condición de carrera que podría dar lugar a caídas en situaciones de carga">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2019 4546 openjdk-11>
<dsa 2019 4563 webkit2gtk>
<dsa 2019 4564 linux>
<dsa 2019 4564 linux-signed-i386>
<dsa 2019 4564 linux-signed-arm64>
<dsa 2019 4564 linux-signed-amd64>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4566 qemu>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4570 mosquitto>
<dsa 2019 4571 enigmail>
<dsa 2019 4571 thunderbird>
<dsa 2019 4572 slurm-llnl>
<dsa 2019 4573 symfony>
<dsa 2019 4575 chromium>
<dsa 2019 4577 haproxy>
<dsa 2019 4578 libvpx>
<dsa 2019 4579 nss>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4583 spip>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4586 ruby2.5>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4595 debian-lan-config>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4599 wordpress>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4605 openjdk-11>
<dsa 2020 4606 chromium>
<dsa 2020 4607 openconnect>
<dsa 2020 4608 tiff>
<dsa 2020 4609 python-apt>
<dsa 2020 4610 webkit2gtk>
<dsa 2020 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4613 libidn2>
<dsa 2020 4615 spamassassin>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction caml-crush "[armel] No se puede compilar debido a la falta de ocaml-native-compilers">
<correction firetray "Incompatible con versiones actuales de Thunderbird">
<correction koji "Problemas de seguridad">
<correction python-lamson "Roto por cambios en python-daemon">
<correction radare2 "Problemas de seguridad; el proyecto original no proporciona soporte estable">
<correction radare2-cutter "Depende de radare2, que se eliminará">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


