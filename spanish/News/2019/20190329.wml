#use wml::debian::translation-check translation="b814e5f8f89e2086e558034fbf26a53d5f985512"
<define-tag pagetitle>Handshake dona 300.000 dólares USD a Debian</define-tag>
<define-tag release_date>2019-03-29</define-tag>
#use wml::debian::news

<p>
En 2018 el proyecto Debian recibió una donación de 300.000 dólares USD de
<a href="https://handshake.org/">Handshake</a>, una organización que desarrolla un
sistema de nombres de dominio raíz entre pares («peer-to-peer») experimental.
</p>

<p>
Esta significativa contribución financiera ayudará a Debian a continuar con el
plan de sustitución de hardware diseñado por los
<a href="https://wiki.debian.org/Teams/DSA">administradores del sistema Debian (DSA)</a>,
renovando servidores y otros componentes hardware y, por lo tanto, haciendo más
fiable la infraestructura de desarrollo y de comunidad del proyecto.
</p>

<p>
<q><em>Un profundo y sincero gracias a la Fundación Handshake por su
apoyo al proyecto Debian</em></q>, dijo Chris Lamb, líder del proyecto Debian.
<q><em>Contribuciones como la suya hacen posible que un gran número de
contribuidoras y contribuidores diversos de todo el mundo trabajen juntos hacia nuestro
objetivo común de desarrollar un sistema operativo <q>universal</q> totalmente libre</em></q>.
</p>

<p>
Handshake es un protocolo de nombres descentralizado, sin permisos y compatible
con DNS, en el que cada par valida y gestiona la
zona raíz con el objetivo de crear una alternativa a las autoridades
certificadoras existentes.
</p>

<p>
El proyecto Handshake, sus patrocinadores y contribuidores reconocen al software
libre y de código abierto como una parte crucial de los fundamentos de
Internet y de su proyecto, por lo que decidieron reinvertir en software
libre entregando 10.200.000 dólares USD a varios desarrolladores y proyectos FLOSS,
así como a organizaciones y universidades sin ánimo de lucro que apoyan el desarrollo de software libre.
</p>

<p>
¡Muchas gracias por el apoyo, Handshake!
</p>

<h2>Acerca de Debian</h2>
<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>
<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a> o envíe un correo electrónico a
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
