#use wml::debian::translation-check translation="96c05360e385187167f1ccbce37d38ce2e5e6920" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.3</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о третьем обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction alot "Удаление даты окончания срока действия у ключей из тестового набора, исправление ошибки сборки">
<correction atril "Исправление ошибки сегментирования, возникающей в случае, если не загружен документ; исправление чтения из неинициализированной области памяти [CVE-2019-11459]">
<correction base-files "Обновление для текущей редакции">
<correction beagle "Предоставление обёрточного сценария вместо символьных ссылок на JAR-файлы, благодаря чему они снова работают">
<correction bgpdump "Исправление ошибки сегментирования">
<correction boost1.67 "Исправление неопределённого поведения, приводящего к аварийной остановке libboost-numpy">
<correction brightd "Фактическое сравнение значения, считываемого из /sys/class/power_supply/AC/online, с <q>0</q>">
<correction casacore-data-jplde "Добавление таблиц вплоть до 2040">
<correction clamav "Новый выпуск основной ветки разработки; исправление отказа в обслуживании [CVE-2019-15961]; удаление опции ScanOnAccess, заменённой на clamonacc">
<correction compactheader "Новый выпуск основной ветки разработки, совместимый с Thunderbird 68">
<correction console-common "Исправление регрессии, приводящей в тому, что файлы не загружаются">
<correction csh "Исправление ошибки сегментирования при выполнении eval">
<correction cups "Исправление утечки памяти в ppdOpen; исправление проверки языка по умолчанию в ippSetValuetag [CVE-2019-2228]">
<correction cyrus-imapd "Добавление типа BACKUP к cyrus-upgrade-db, что исправляет проблемы с обновлением">
<correction debian-edu-config "Сохранение настроек прокси у клиента, если недоступен WPAD">
<correction debian-installer "Повторная сборка с учётом пакетов из proposed-updates; настройка создания mini.iso на arm, чтобы заработала сетевая загрузка в EFI; обновление значения USE_UDEBS_FROM по умолчанию с unstable на buster, чтобы помочь пользователям выполнять локальные сборки">
<correction debian-installer-netboot-images "Повторная сборка с учётом пакетов из proposed-updates">
<correction debian-security-support "Обновление статуса поддержки безопасности некоторых пакетов">
<correction debos "Повторная сборка с учётом пакета golang-github-go-debos-fakemachine">
<correction dispmua "Новый выпуск основной ветки разработки, совместимый с Thunderbird 68">
<correction dkimpy "Новый стабильный выпуск основной ветки разработки">
<correction dkimpy-milter "Исправление управления привилегиями при запуске для работы Unix-сокетов">
<correction dpdk "Новый стабильный выпуск основной ветки разработки">
<correction e2fsprogs "Исправление потенциального отрицательного переполнения стека в e2fsck [CVE-2019-5188]; исправление использования указателей после освобождения памяти в e2fsck">
<correction fig2dev "Разрешение текстовым строкам Fig v2 заканчиваться на несколько символов ^A [CVE-2019-19555]; отклонение типов огромных стрелок, вызывающих переполнение целых чисел [CVE-2019-19746]; исправление нескольких аварийных остановок [CVE-2019-19797]">
<correction freerdp2 "Исправление обработки возвращения в realloc [CVE-2019-17177]">
<correction freetds "tds: проверка, что у UDT значением varint является 8 [CVE-2019-13508]">
<correction git-lfs "Исправление сборки с новыми версиями Go">
<correction gnubg "Увеличение размера статичных буферов, используемых для сборочных сообщений в ходе запуска программы, чтобы перевод на испанский язык не переполнял буфер">
<correction gnutls28 "Исправление проблем interop с gnutls 2.x; исправление грамматического разбора сертификатов, использующих RegisteredID">
<correction gtk2-engines-murrine "Исправление совместной установки с другими темами">
<correction guile-2.2 "Исправление ошибки сборки">
<correction libburn "Исправление ошибки <q>медленный прожиг нескольких треков cdrskin, остановка после трека 1</q>">
<correction libcgns "Исправление ошибки сборки на ppc64el">
<correction libimobiledevice "Корректная обработка частичных записей SSL">
<correction libmatroska "Номер версии зависимости разделяемой библиотеки увеличен до 1.4.7, поскольку в этой версии были добавлены новые символы">
<correction libmysofa "Исправления безопасности [CVE-2019-16091 CVE-2019-16092 CVE-2019-16093 CVE-2019-16094 CVE-2019-16095]">
<correction libole-storage-lite-perl "Исправление интерпретации годов с 2020 и далее">
<correction libparse-win32registry-perl "Исправление интерпретации годов с 2020 и далее">
<correction libperl4-corelibs-perl "Исправление интерпретации годов с 2020 и далее">
<correction libsolv "Исправление переполнения буфера [CVE-2019-20387]">
<correction libspreadsheet-wright-perl "Исправление ранее неработающих электронных таблиц OpenDocument и передача опций форматирования JSON">
<correction libtimedate-perl "Исправление интерпретации годов с 2020 и далее">
<correction libvirt "Apparmor: разрешение запускать pygrub; не передавать osxsave, ospke в командную строку QEMU; это помогает новым версиям QEMU с некоторыми настройками, созданными virt-install">
<correction libvncserver "RFBserver: не раскрывать стековую память удалённой системе [CVE-2019-15681]; разрешение заморозки во время закрытия соединения и ошибки сегментирования на  многопоточных VNC-серверах; исправление проблемы подключения к серверам VMWare; исправление аварийной остановки x11vnc при подключении vncviewer">
<correction limnoria "Исправление удалённого раскрытия информации и возможного удалённого выполнения кода в дополнении Math [CVE-2019-19010]">
<correction linux "Новый стабильный выпуск основной ветки разработки">
<correction linux-latest "Обновление с учётом ABI ядра Linux версии 4.19.0-8">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки">
<correction mariadb-10.3 "Новый стабильный выпуск основной ветки разработки [CVE-2019-2938 CVE-2019-2974 CVE-2020-2574]">
<correction mesa "Вызов shmget() с правами 0600 вместо 0777 [CVE-2019-5068]">
<correction mnemosyne "Добавление отсутствующей зависимости от PIL">
<correction modsecurity "Исправление ошибки грамматического разбора заголовка куки [CVE-2019-19886]">
<correction node-handlebars "Запрет прямого вызова <q>helperMissing</q> и <q>blockHelperMissing</q> [CVE-2019-19919]">
<correction node-kind-of "Исправление проблем с проверкой типов в ctorName() [CVE-2019-20149]">
<correction ntpsec "Исправление медленных повторных попыток обращения к DNS; исправление ntpdate -s (syslog), чтобы исправить перехватчик if-up; исправления документации">
<correction numix-gtk-theme "Исправление совместной установки с другими темами">
<correction nvidia-graphics-drivers-legacy-340xx "Новый стабильный выпуск основной ветки разработки">
<correction nyancat "Повторная сборка в чистом окружении, чтобы добавить юнит systemd для nyancat-server">
<correction openjpeg2 "Исправление переполнения кучи [CVE-2018-21010] и переполнения целых чисел [CVE-2018-20847]">
<correction opensmtpd "Предупреждение пользователей об изменении синтаксиса smtpd.conf (в более ранних версиях); установка smtpctl setgid opensmtpq; обработка ненулевого кода выхода из hostname в фазе настройки">
<correction openssh "Отклонение (не фатального) ipc в песочнице seccomp, исправляющее ошибки с OpenSSL 1.1.1d и Linux &lt; 3.19 на некоторых архитектурах">
<correction php-horde "Исправление межсайтового скриптинга в Horde Cloud Block [CVE-2019-12095]">
<correction php-horde-text-filter "Исправление неправильных регулярных выражений">
<correction postfix "Новый стабильный выпуск основной ветки разработки">
<correction postgresql-11 "Новый стабильный выпуск основной ветки разработки">
<correction print-manager "Исправление аварийной остановки, если CUPS возвращает один и тот же идентификатор для нескольких задач печати">
<correction proftpd-dfsg "Исправление проблем CRL [CVE-2019-19270 CVE-2019-19269]">
<correction pykaraoke "Исправление пути к шрифтам">
<correction python-evtx "Исправление импортирования <q>hexdump</q>">
<correction python-internetarchive "Закрытие файла после получения хеша, что позволяет избежать исчерпания файловых дескрипторов">
<correction python3.7 "Исправления безопасности [CVE-2019-9740 CVE-2019-9947 CVE-2019-9948 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935]">
<correction qtbase-opensource-src "Добавление поддержки для принтеров без PPD и предотвращение молчаливого отката к принтеру, поддерживающему PPD; исправление аварийной остановки при использовании QLabels с форматированным текстом; исправление графики событий наведения указателя мыши на таблицу">
<correction qtwebengine-opensource-src "Исправление грамматического разбора PDF; отключение исполняемого стека">
<correction quassel "Исправление отказов AppArmor quasselcore при сохранении настройки; правильный канал по умолчанию для Debian; удаление ненужного файла NEWS">
<correction qwinff "Исправление аварийной остановки из-за неправильного определения файла">
<correction raspi3-firmware "Исправление определения последовательной консоли с ядром 5.x">
<correction ros-ros-comm "Исправление проблем безопасности [CVE-2019-13566 CVE-2019-13465 CVE-2019-13445]">
<correction roundcube "Новый стабильный выпуск основной ветки разработки; исправление небезопасных прав доступа в дополнении enigma [CVE-2018-1000071]">
<correction schleuder "Исправление определения ключевых слов в почтовых сообщениях с <q>защищёнными заголовками</q> и пустой темой; удаление несобственных подписей при обновлении или загрузке ключей; вывод ошибки в случае, если аргумент, переданный `refresh_keys`, не является существующим списком; добавление отсутствующего заголовка List-Id к уведомлениям, отправляемым администраторам; вежливая обработка проблем расшифровки; использование кодировки ASCII-8BIT по умолчанию">
<correction simplesamlphp "Исправление несовместимости с PHP 7.3">
<correction sogo-connector "Новый выпуск основной ветки разработки, совместимый с Thunderbird 68">
<correction spf-engine "Исправление управления привилегиями во время запуска для работы Unix-сокетов; обновление документации для TestOnly">
<correction sudo "Исправление (не доступного для использования в buster) переполнения буфера, если включён pwfeedback, а ввод осуществляется не через tty [CVE-2019-18634]">
<correction systemd "Установка fs.file-max sysctl в значение LONG_MAX, а не ULONG_MAX; изменение владельца/режима каталогов выполнения и для статичных пользователей, что гарантирует, что владельцем каталогов выполнения, таких как CacheDirectory и StateDirectory, правильно устанавливается в значение пользователь, указанный в директиве User= до запуска службы">
<correction tifffile "Исправление обёрточного сценария">
<correction tigervnc "Исправления безопасности [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Исправления безопасности [CVE-2014-6053 CVE-2019-8287 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction uif "Исправление путей к ip(6)tables-restore в свете перехода на nftables">
<correction unhide "Исправление исчерпания стека">
<correction x2goclient "Удаление ~/, ~user{,/}, ${HOME}{,/} и $HOME{,/} из целевых путей в режиме SCP; исправление регрессии, возникающей с новыми версиями libssh и исправлениями для CVE-2019-14889">
<correction xmltooling "Исправление состояния гонки, приводящего к аварийной остановке при нагрузке на систему">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2019 4546 openjdk-11>
<dsa 2019 4563 webkit2gtk>
<dsa 2019 4564 linux>
<dsa 2019 4564 linux-signed-i386>
<dsa 2019 4564 linux-signed-arm64>
<dsa 2019 4564 linux-signed-amd64>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4566 qemu>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4570 mosquitto>
<dsa 2019 4571 enigmail>
<dsa 2019 4571 thunderbird>
<dsa 2019 4572 slurm-llnl>
<dsa 2019 4573 symfony>
<dsa 2019 4575 chromium>
<dsa 2019 4577 haproxy>
<dsa 2019 4578 libvpx>
<dsa 2019 4579 nss>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4583 spip>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4586 ruby2.5>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4595 debian-lan-config>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4599 wordpress>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4605 openjdk-11>
<dsa 2020 4606 chromium>
<dsa 2020 4607 openconnect>
<dsa 2020 4608 tiff>
<dsa 2020 4609 python-apt>
<dsa 2020 4610 webkit2gtk>
<dsa 2020 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4613 libidn2>
<dsa 2020 4615 spamassassin>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за обстоятельств, на которые мы не
можем повлиять:</p>


<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction caml-crush "[armel] Не может быть собран из-за отсутствия пакета ocaml-native-compilers">
<correction firetray "Несовместим с текущими версиями Thunderbird">
<correction koji "Проблемы безопасности">
<correction python-lamson "Сломан из-за изменений в python-daemon">
<correction radare2 "Проблемы безопасности; основная ветка разработки не предоставляет стабильную поддержку">
<correction radare2-cutter "Зависимость от удаляемого пакета radare2">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
