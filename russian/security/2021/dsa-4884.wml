#use wml::debian::translation-check translation="8841a09ee24923f86538151f0ba51430b8414c1a" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В ldb, LDAP-подобной встраиваемой базе данных, надстроенной над TDB,
были обнаружены многочисленные уязвимости.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10730">CVE-2020-10730</a>

    <p>Эндрю Бартлет обнаружил разыменование NULL-указателя и использование указателей
    после освобождения памяти в коде обработки управляющих элементов LDAP <q>ASQ</q> и
    <q>VLV</q> и их комбинаций с paged_results.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27840">CVE-2020-27840</a>

    <p>Дуглас Багнал обнаружил повреждение содержимого динамической памяти с помощью
    специально сформированных DN-строк.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20277">CVE-2021-20277</a>

    <p>Дуглас Багнал обнаружил чтение за пределами выделенного буфера памяти в коде
    обработки атрибутов LDAP, содержащих последовательности из нескольких пробельных
    символов в начале значения атрибута.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 2:1.5.1+really1.4.6-3+deb10u1.</p>

<p>Рекомендуется обновить пакеты ldb.</p>

<p>С подробным статусом поддержки безопасности ldb можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/ldb">\
https://security-tracker.debian.org/tracker/ldb</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4884.data"
