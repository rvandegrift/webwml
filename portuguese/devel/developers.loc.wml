#use wml::debian::template title="Localização dos(as) desenvolvedores(as)"
#use wml::debian::translation-check translation="6c996020eca890cac2a0243d4633d9bfa7935674"

<p>Muitas pessoas demostram interesse em informações sobre a
localização dos(as) desenvolvedores(as) Debian.
Assim, decidimos adicionar como parte do banco de dados dos(as)
desenvolvedores(as) um campo onde os(as) desenvolvedores(as) podem especificar
suas coordenadas no mundo.

<p>O mapa abaixo foi gerado de uma anonimizada
<a href="developers.coords">lista de coordenadas de desenvolvedores(as)</a>
usando o programa
<a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a>.

<p><img src="developers.map.jpeg" alt="World Map">

<p>Se você é um(a) desenvolvedor(a) e gostaria de adicionar suas coordenadas
para sua entrada do banco de dados, faça o login no
<a href="https://db.debian.org">banco de dados dos(as) desenvolvedores(as) Debian</a>
e modifique sua entrada. Se você não sabe
as coordenadas de sua cidade, você deve achá-la em uma das seguintes
localizações:
<ul>
<li><a href="https://osm.org">Openstreetmap</a>
Você pode procurar sua cidade na barra de pesquisa.
Selecione as setas de direção ao lado da barra de pesquisa e, em seguida,
arraste o marcador verde para o mapa OSM. As coordenadas aparecerão na caixa
'De'.
</ul>

<p>O formato das coordenadas é um dos seguintes:
<dl>
<dt>Graus decimais
<dd>O formato é +-DDD.DDDDDDDDDDDDDDD.  Esse é o formato
    que programas como o xearth usam e o formato que
    muitos sites de posicionamento usam. No entanto,
    a precisão é limitada a 4 ou 5 decimais.
<dt>Graus Minutos (DGM)
<dd>O formato é +-DDDMM.MMMMMMMMMMMMM. Não é de tipo
    aritmético, mas uma representação empacotada
    de duas unidades separadas: graus e minutos. Essa
    saída é comum para alguns tipos de unidades GPS
    de mão e mensagens GPS no formato NMEA.
<dt>Graus Minutos Segundos (DGMS)
<dd>O formato é +-DDDMMSS.SSSSSSSSSSS. Como o DGM, ele não
    é um tipo aritmético, mas uma representação empacotada
    de três unidades separadas: graus, minutos e segundos.
    Essa saída é tipicamente derivada de sites web que
    dão três valores para cada posição. Por exemplo
    34:50:12.24523 Norte pode ser a posição dada, em
    DGMS isso seria +0345012.24523.
</dl>

<p>
Para latitude + é Norte, para longitude + é Leste. É importante
especificar zeros iniciais o suficiente para tornar claro o
formato que está sendo usado, se sua posição é menos dois graus
de um ponto zero.
