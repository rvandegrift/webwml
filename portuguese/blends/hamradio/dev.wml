#use wml::debian::blend title="Desenvolvimento do blend"
#use "navbar.inc"
#use wml::debian::translation-check translation="c3a3b8986d2ffef95ef7bb8f7d99f36678ff0e8f"

<h2>Empacotamento</h2>

<p>Enquanto o empacotamento do software de rádio amador no Debian é de
responsabilidade dos(as)
<a href="https://pkg-hamradio.alioth.debian.org">mantenedores(as) do Debian Hamradio</a>,
o projeto do pure blend fornece as seguintes ferramentas que podem ser úteis
para empacotadores(as):</p>

<ul>
	<li><a href="https://blends.debian.org/hamradio/tasks/">Índice de tarefas do blend</a></li>
	<li><a href="https://blends.debian.org/hamradio/bugs/">Visão geral dos bugs do blend</a></li>
	<li><a href="https://blends.debian.org/hamradio/thermometer/">Termômetro do blend</a></li>
</ul>

<h2>Código-fonte dos metapacotes</h2>

<ul>
	<li><a href="https://salsa.debian.org/blends-team/hamradio">Navegador git baseado em web</a></li>
	<li><a href="https://salsa.debian.org/blends-team/hamradio.git">Git clone anônimo</a></li>
</ul>

<pre>git clone https://salsa.debian.org/blends-team/hamradio
cd hamradio ; make dist
gbp buildpackage</pre>

<p><i>Nota: você precisará de <a
href="https://packages.debian.org/unstable/git-buildpackage">git-buildpackage</a>
e <a href="https://packages.debian.org/unstable/blends-dev">blends-dev</a>
instalados para construir o código-fonte.</i></p>

<p>Para aprender mais sobre como trabalhar com metapacotes de blends, veja o
<a href="https://blends.debian.org/blends/ch06.html#metapackages">§6.1</a> do
<a href="https://blends.debian.org/blends/">manual de pure blends Debian</a>.</p>

<h2>Código-fonte do DVD live</h2>

<ul>
	<li><a href="https://salsa.debian.org/blends-team/blends-images">Navegador git baseado em web</a></li>
	<li><a href="https://salsa.debian.org/blends-team/blends-images.git">Git clone anônimo</a></li>
</ul>

<pre>git clone https://salsa.debian.org/blends-team/blends-images.git
cd blends-images/images/hamradio-amd64
lb config
sudo lb build</pre>

<p><i>Nota: você precisará de <a
 href="https://packages.debian.org/unstable/git-buildpackage">git-buildpackage</a>
	e <a href="https://packages.debian.org/unstable/live-build">live build</a>
        instalados para construir o código-fonte.</i></p>

<p>Para aprender mais sobre como trabalhar com fontes live-build, veja o <a
 href="http://live.debian.net/manual/unstable/html/live-manual/toc.en.html">manual
 de sistemas live</a>.</p>
