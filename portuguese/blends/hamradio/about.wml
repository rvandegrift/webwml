#use wml::debian::blend title="Sobre o Blend"
#use "navbar.inc"
#use wml::debian::translation-check translation="6d2e0afb56e760364713c2cca2c9f6407c9a744f"

<p>O <b>Pure Blend Debian Hamradio</b> é um projeto do <a
href="https://wiki.debian.org/DebianHams/">time de mantenedores(as) do Debian
Hamradio</a> que colabora na manutenção de pacotes relacionados ao
radioamadorismo para o Debian.
Todo <a href="https://blends.debian.org/">Pure Blend</a> é um subconjunto do
Debian que, configurado e pronto logo após a instalação, suporta um grupo-alvo.
Este Blend objetiva apoiar as demandas de radioamadoristas.</p>

<p>O Blend é construído de uma lista, com curadoria, de software de rádio
amador no Debian. Os dois principais produtos do blend são uma coleção de
"metapacotes" e imagens live que podem ser colocadas em DVD ou em pendrive
usb.</p>

<h2>Metapacotes</h2>

<p>Metapacotes no Debian são pacotes que podem ser instalados como qualquer
outro, mas que não contêm, em si mesmos, qualquer software. Em vez disso,
instruem o sistema de empacotamento a instalar um grupo de outros pacotes.</p>

<p>Veja <a href="./get/metapackages">usando metapacotes</a> para mais
informações sobre quais metapacotes estão disponíveis e como instalá-los
em um sistema Debian existente.</p>

<h2>Imagens live</h2>

<p>Se você é iniciante no Debian e gostaria de tentar o Pure Blend Debian
Hamradio sem instalá-lo em seu computador, ou se desejar fazer uma nova
instalação do Debian com todo o software radioamador pronto para uso,
as imagens live podem ser úteis. As imagens são produzidas de modo a
serem executadas de DVDs e pendrives tanto em arquiteturas Intel 32-bit como
64-bit.</p>

<p>Veja a <a href="./get/live">página de imagens live</a> para mais
informações.</p>
