#use wml::debian::template title="Debians verdensomspændende filspejle" BARETITLE=true
#use wml::debian::translation-check translation="54e7b1c853358a6c386f31e4ac3a5d90a6f3890f"

<p>Debian distribueres (<em>spejles</em>, <em>mirrored</em>) på hundredvis af 
servere på internettet.  Ved at benytte en server, som befinder sig tæt på dig, 
vil det formentlig gå hurtigere at hente filer, og det vil reducere belastningen 
på vores centrale servere og internettet i det hele taget.</p>

<p class="centerblock">
    Debian har filspejle i mange lande, og til nogle har vi tiløjet et 
    <code>ftp.&lt;land&gt;.debian.org</code>-alias.  Dette alias peger normalt 
    på et filspejl, der synkroniserer jævnligt og hurtigt, og opbevarer alle 
    Debians arkitekturer.  Debians arkiv er altid tilgængeligt via 
    <code>HTTP</code> i mappen <code>/debian</code> på serveren.
</p>

<p class="centerblock">
    Andre <strong>filspejle</strong> kan have begrænsninger på hvad de spejler 
    (på grund af pladsbegrænsninger).  Blot fordi et filspejl ikke er et lands 
    <code>ftp.&lt;land&gt;.debian.org</code>, betyder det ikke nødvendigvis at 
    det er spor langsommere eller mindre ajourført end filspejlet 
    <code>ftp.&lt;land&gt;.debian.org</code>.  Faktisk vil et filspejl, der 
    stiller din arkitektur til rådighed og er tættere på dig som bruger, være 
    hurtigere og altid at foretrække frem for at primært, der er længere væk.
</p>

<p>Benyt det sted, som er tættest på dig, for at opnå de højste 
downloadhastigheder, uanset om det har et landealias eller ej.
Programmet <a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a> kan anvendes til at afgøre, hvilket sted har den laveste 
forsinkelse (latency); benyt et downloadprogram så som 
<a href="https://packages.debian.org/stable/web/wget"><em>wget</em></a> eller
<a href="https://packages.debian.org/stable/net/rsync"><em>rsync</em></a> for at 
afgøre hvilket sted, der giver den højeste hastighed.  Bemærk at geografisk 
nærhed ikke er den vigtige faktor i at afgøre hvilken server, der vil give dig 
det bedste resultat.</p>

<p>Hvis dit system ofte flyttes, kan du måske være bedst tjent med et filspejl, 
der bag sig har et globalt <abbr title="Content Delivery Network">CDN</abbr>.  
Til det formål vedligeholder Debian-projektet <code>deb.debian.org</code>, som 
kan anvendes i din apt sources.list &ndash; se <a href="http://deb.debian.org/">\
tjenestens websted for flere oplysninger</a>.</p>

<p>Den autoritative udgave af følgende liste finder man altid på:
<url "https://www.debian.org/mirror/list">. 
Alt andet hvad man har brug for at vide om Debian-filspejle:
<url "https://www.debian.org/mirror/">.
</p>


<h2 class="center">Debians filspejlaliaser pr. land</h2>

<table border="0" class="center">
<tr>
  <th>Land</th>
  <th>Sted</th>
  <th>Arkitekturer</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>


<h2 class="center">Liste over filspejle med Debians arkiv</h2>

<table border="0" class="center">
<tr>
  <th>Værtsnavn</th>
  <th>HTTP</th>
  <th>Arkitekturer</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
