# Jorge Barreiro <yortx.barry@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2012-12-09 20:21+0100\n"
"Last-Translator: Jorge Barreiro <yortx.barry@gmail.com>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Estatísticas de tradución do sitio web de Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Hai %d páxinas por traducir."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Hai %d bytes por traducir."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Hai %d cadeas por traducir."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "A versión da tradución é incorrecta"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Esta tradución está demasiado desfasada"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "O orixinal é máis recente que esta tradución"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "O orixinal xa non existe"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "non hai número de visitas"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "visitas"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Pulse para obter datos de «diffstat»"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Resumo da tradución para o"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Sen traducir"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Desfasado"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Traducido"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Actualizado"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "ficheiros"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Nota: as listas de páxinas están ordenadas por popularidade. Pase por riba "
"do nome da páxina para ver o número de visitas."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Traducións desfasadas"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Ficheiro"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Diferenzas"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Comentario"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Rexistro"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Tradución"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Mantedor"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Estado"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Tradutor"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Data"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "As páxinas xerais non están traducidas"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Páxinas xerais sen traducir"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "As novas non están traducidas"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Novas non traducidas"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "As páxinas de usuarios/asistentes técnicos non están traducidas"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Páxinas de asistentes técnicos/usuarios non traducidas"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "As páxinas internacionais non están traducidas"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Páxinas internacionais non traducidas"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Páxinas traducidas (actualizadas)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Modelos traducidos (ficheiros PO)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "Estatísticas de traducións PO"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Dubidosas"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Sen traducir"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Total:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Páxinas web traducidas"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Estatísticas de tradución por número de páxinas"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Idioma"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Traducións"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Páxinas web traducidas (por tamaño)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Estatísticas de tradución por tamaño de páxina"

#~ msgid "Unified diff"
#~ msgstr "«diff» unificado"

#~ msgid "Colored diff"
#~ msgstr "«diff» coloreado"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "«diff» coloreado"

#~ msgid "Created with"
#~ msgstr "Creado con"
