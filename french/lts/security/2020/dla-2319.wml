#use wml::debian::translation-check translation="267a30bb67d9f1c7c655c00ffc34e64a4b30c4a2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le service xrdp-sesman dans xrdp pouvait être planté par une connexion sur le
port 3350 et une fourniture de charge malveillante. Une fois que le processus
xrdp-sesman était mort, un attaquant sans privilèges pouvait démarrer son
propre service usurpateur sesman écoutant sur le port 3350. Cela permettait de
capturer toutes les accréditations d’utilisateur soumises à XRDP et approuver ou
rejeter des accréditations de connexion arbitraires. Pour les sessions xorgxrdp
en particulier, cela permettait à un utilisateur non autorisé de détourner une
session existante. C’est une attaque par dépassement de tampon, donc un risque
d’exécution de code arbitraire existait en plus de cela.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.9.1-9+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xrdp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xrdp, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xrdp">https://security-tracker.debian.org/tracker/xrdp</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2319.data"
# $Id: $
