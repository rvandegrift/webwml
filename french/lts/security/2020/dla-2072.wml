#use wml::debian::translation-check translation="2ed3034db4ede1eb0838482db0c18c7fa5020f7f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été trouvés dans gpac, un cadriciel multimédia
fournissant le multiplexeur MP4Box.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-21015">CVE-2018-21015</a>

<p>AVC_DuplicateConfig() dans isomedia/avc_ext.c permet à des attaquants
distants de provoquer un déni de service (déréférencement de pointeur NULL et
plantage d'application) à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-21016">CVE-2018-21016</a>

<p>audio_sample_entry_AddBox() dans isomedia/box_code_base.c permet à des
attaquants distants de provoquer un déni de service (lecture hors limites de
tampon de tas et plantage d'application) à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13618">CVE-2019-13618</a>

<p>isomedia/isom_read.c dans libgpac.a renferme une lecture hors limites de
tampon de tas, comme démontré par un plantage dans gf_m2ts_sync dans
media_tools/mpegts.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20161">CVE-2019-20161</a>

<p>Dépassement de tampon de tas dans la fonction ReadGF_IPMPX_WatermarkingInit()
dans odf/ipmpx_code.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20162">CVE-2019-20162</a>

<p>Dépassement de tampon de tas dans la fonction gf_isom_box_parse_ex() dans
isomedia/box_funcs.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20163">CVE-2019-20163</a>

<p>Déréférencement de pointeur NULL dans la fonction gf_odf_avc_cfg_write_bs()
dans odf/descriptors.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20165">CVE-2019-20165</a>

<p>Déréférencement de pointeur NULL dans la fonction ilst_item_Read() dans
isomedia/box_code_apple.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20170">CVE-2019-20170</a>

<p>Déréférencement non valable de pointeur dans la fonction
GF_IPMPX_AUTH_Delete() dans odf/ipmpx_code.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20171">CVE-2019-20171</a>

<p>Fuite de mémoire dans metx_New dans isomedia/box_code_base.c et abst_Read
dans isomedia/box_code_adobe.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20208">CVE-2019-20208</a>

<p>dimC_Read dans isomedia/box_code_3gpp.c dans GPAC 0.8.0 renferme un
dépassement de pile.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.5.0+svn5324~dfsg1-1+deb8u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gpac.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2072.data"
# $Id: $
