#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans spice, une bibliothèque de
client et serveur de protocole SPICE. Le projet « Common vulnérabilités et
Exposures » (CVE) identifie les problèmes suivants :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9577">CVE-2016-9577</a>

<p>Frediano Ziglio de Red Hat a découvert une vulnérabilité de dépassement de
tampon dans la fonction main_channel_alloc_msg_rcv_buf. Un attaquant authentifié
peut exploiter ce défaut pour provoquer un déni de service (plantage du serveur
spice), ou possiblement, exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9578">CVE-2016-9578</a>

<p>Frediano Ziglio de Red Hat a découvert que spice ne validait pas correctement
les messages entrants. Un attaquant capable de se connecter au serveur spice
pourrait envoyer des messages contrefaits qui pourraient causer le plantage du
processus.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.11.0-1+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets spice.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-825.data"
# $Id: $
