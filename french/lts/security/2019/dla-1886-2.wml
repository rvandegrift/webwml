#use wml::debian::translation-check translation="e1f2c2bb957cc4424b1f3e51c27e1fe9867f0118" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La dernière mise à jour de sécurité de openjdk-7 causait une régression
lorsque les applications reposaient sur les algorithmes utilisant les courbes
elliptiques pour établir les connexions SSL. Plusieurs classes dupliquées ont
été supprimées de rt.jar par les développeurs amont d’OpenJDK parce qu’elles
étaient aussi présentes dans sunec.jar. Cependant, Debian n’embarquait pas le
fournisseur de sécurité SunEC dans OpenJDK 7.</p>

<p>Le problème a été corrigé en construisant sunec.jar et sa bibliothèque
native correspondante libsunec.so à partir du source. Pour construire ces
bibliothèques à partir du source, une mise à niveau vers la
version 2:3.26-1+debu8u6 est nécessaire.</p>

<p>Les mises à jour pour l’architecture amd64 sont déjà disponibles, les
nouveaux paquets pour i386, armel et armhf seront disponibles sous 24 heures.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 7u231-2.6.19-1~deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets openjdk-7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1886-2.data"
# $Id: $
