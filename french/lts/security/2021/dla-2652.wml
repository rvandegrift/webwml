#use wml::debian::translation-check translation="04f95535bee7ec86481fbdbb62cd3d2c3fe6caff" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Unbound, un
résolveur DNS validateur, cache et récursif par des chercheurs en sécurité de
X41 D-SEC situé à Aachen en Allemagne. Des dépassements d'entier, des échecs
d’assertion, des écritures hors limites et une vulnérabilité de boucle infinie
pourraient conduire à un déni de service ou avoir un impact négatif sur la
confidentialité des données.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.9.0-2+deb10u2~deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets unbound1.9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de unbound1.9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/unbound1.9">\
https://security-tracker.debian.org/tracker/unbound1.9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2652.data"
# $Id: $
