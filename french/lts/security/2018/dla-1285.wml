#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>BIND, une implémentation de serveur DNS, était vulnérable à un défaut
de déni de service découvert dans le traitement de validations DNSSEC. Un
attaquant distant pourrait utiliser ce défaut faire s'interrompre le démon
named de façon inattendue avec un échec d'assertion à l'aide d'une réponse
DNS contrefaite pour l'occasion. Ce problème est étroitement lié au <a
href="https://security-tracker.debian.org/tracker/CVE-2017-3139">CVE-2017-3139</a>.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 9.8.4.dfsg.P1-6+nmu2+deb7u20.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1285.data"
# $Id: $
