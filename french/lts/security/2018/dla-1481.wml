#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs chercheurs ont découvert une vulnérabilité dans la manière dont la
conception du processeur Intel a implémenté l’exécution spéculative
d’instructions en combinaison avec le traitement d’erreur de page. Ce défaut
pourrait permettre à un attaquant contrôlant un processus sans droits de lire la
mémoire à partir d’adresses arbitraires (contrôlées par un non utilisateur),
incluant celles du noyau et de tous les autres processus en cours d’exécution
sur le système ou traverser les limites invité/hôte pour lire la mémoire de
l’hôte.</p>

<p>Pour complètement remédier à ces vulnérabilités, il est aussi nécessaire
d’installer le microcode du CPU (seulement disponible dans Debian non-free). Les
CPU courants de la classe serveur sont couverts par la mise à jour publiée avec
DLA 1446-1.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.110-3+deb9u4~deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets linux-4.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1481.data"
# $Id: $
