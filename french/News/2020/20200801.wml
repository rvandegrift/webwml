#use wml::debian::translation-check translation="846effa858f46cf6429f61e241ec96292032972f" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.5</define-tag>
<define-tag release_date>2020-08-01</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la cinquième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Cette version intermédiaire corrige aussi l'<a
href="$(HOME)/security/2020/dsa-4735">annonce de sécurité de Debian
DSA-4735 grub2</a> qui traite de plusieurs problèmes de CVE concernant la <a
href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot/">vulnérabilité
<q>BootHole</q> de UEFI SecureBoot dans GRUB2</a>.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version
de Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
la version <codename>. Après installation, les paquets peuvent être mis
à niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>Cette mise à jour de la version stable apporte quelques corrections
importantes aux paquets suivants :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction appstream-glib "Correction d'échecs de construction en 2020 et après">
<correction asunder "Utilisation de gnudb à la place de freedb par défaut">
<correction b43-fwcutter "Succès assuré des suppressions avec des locales non anglaises ; pas d'échec de retrait si certains fichiers n'existent plus ; correction de dépendances manquantes à pciutils et à ca-certificates">
<correction balsa "Identité du serveur fournie lors de la validation de certificats, permettant une validation réussie lors de l'utilisation du correctif de glib-networking pour le CVE-2020-13645">
<correction base-files "Mise à jour pour cette version">
<correction batik "Correction d'une falsification de requêtes côté serveur au moyen d'attributs xlink:href [CVE-2019-17566]">
<correction borgbackup "Correction d'un bogue de corruption d'index menant à une perte de données">
<correction bundler "Mise à jour de la version requise de ruby-molinillo">
<correction c-icap-modules "Ajout de la prise en charge pour ClamAV 0.102">
<correction cacti "Correction d'un problème où les horodatages UNIX après le 13 septembre 2020 étaient rejetés en début ou fin de graphique ; correction d'exécution de code distant [CVE-2020-7237], de script intersite [CVE-2020-7106], d'un problème de CSRF [CVE-2020-13231] ; la désactivation d'un compte utilisateur n'invalide pas immédiatement ses droits [CVE-2020-13230]">
<correction calamares-settings-debian "Activation du module displaymanager, corrigeant des options d'autologin ; utilisation de xdg-user-dir pour spécifier le répertoire Bureau">
<correction clamav "Nouvelle version amont ; corrections de sécurité [CVE-2020-3327 CVE-2020-3341 CVE-2020-3350 CVE-2020-3327 CVE-2020-3481]">
<correction cloud-init "Nouvelle version amont">
<correction commons-configuration2 "Création d'objet évitée lors du chargement de fichiers YAML [CVE-2020-1953]">
<correction confget "Correction de la gestion de valeurs contenant <q>=</q> par le module Python">
<correction dbus "Nouvelle version amont stable ; problème de déni de service évité [CVE-2020-12049] ; utilisation de mémoire après libération évitée si deux noms d'utilisateur partagent un UID">
<correction debian-edu-config "Correction de perte d'adresse IPv4 dynamiquement allouée">
<correction debian-installer "Mise à jour pour l'ABI du noyau 4.19.0-10">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debian-ports-archive-keyring "Prolongement d'un an de la date d'expiration de la clé de 2020 (84C573CD4E1AFD6C) ; ajout de la clé de signature automatique de l'archive des portages de Debian (2021) ; migration de la clé de 2018 (ID: 06AED62430CB581C) dans le trousseau retiré">
<correction debian-security-support "Mise à jour de l'état de la prise en charge de plusieurs paquets">
<correction dpdk "Nouvelle version amont">
<correction exiv2 "Ajustement d'un correctif excessivement restrictif [CVE-2018-10958 et CVE-2018-10999] ; correction d'un problème de déni de service [CVE-2018-16336]">
<correction fdroidserver "Correction de la validation de l'adresse de Litecoin">
<correction file-roller "Correction de sécurité [CVE-2020-11736]">
<correction freerdp2 "Correction de connexions de smartcard ; corrections de sécurité [CVE-2020-11521 CVE-2020-11522 CVE-2020-11523 CVE-2020-11524 CVE-2020-11525 CVE-2020-11526]">
<correction fwupd "Nouvelle version amont ; correction d'un possible problème de vérification de signature [CVE-2020-10759] ; utilisation de clés de signature de Debian renouvelées après rotation">
<correction fwupd-amd64-signed "Nouvelle version amont ; correction d'un possible problème de vérification de signature [CVE-2020-10759] ; utilisation de clés de signature de Debian renouvelées après rotation">
<correction fwupd-arm64-signed "Nouvelle version amont ; correction d'un possible problème de vérification de signature [CVE-2020-10759] ; utilisation de clés de signature de Debian renouvelées après rotation">
<correction fwupd-armhf-signed "Nouvelle version amont ; correction d'un possible problème de vérification de signature [CVE-2020-10759] ; utilisation de clés de signature de Debian renouvelées après rotation">
<correction fwupd-i386-signed "Nouvelle version amont ; correction d'un possible problème de vérification de signature [CVE-2020-10759] ; utilisation de clés de signature de Debian renouvelées après rotation">
<correction fwupdate "Utilisation de clés de signature de Debian renouvelées après rotation">
<correction fwupdate-amd64-signed "Utilisation de clés de signature de Debian renouvelées après rotation">
<correction fwupdate-arm64-signed "Utilisation de clés de signature de Debian renouvelées après rotation">
<correction fwupdate-armhf-signed "Utilisation de clés de signature de Debian renouvelées après rotation">
<correction fwupdate-i386-signed "Utilisation de clés de signature de Debian renouvelées après rotation">
<correction gist "API d'autorisation obsolète évitée">
<correction glib-networking "Renvoi d'une erreur de mauvaise identité si l'identité n'est pas configurée [CVE-2020-13645] ; balsa plus ancien que la version 2.5.6-2+deb10u1 cassé parce que le correctif pour le CVE-2020-13645 casse la vérification de certificat de balsa">
<correction gnutls28 "Correction d'erreurs de reprise de session TL1.2 ; correction de fuite de mémoire ; prise en charge des tickets de session de longueur nulle, correction d'erreurs de connexion de sessions TLS1.2 vers certains gros fournisseurs d'hébergement ; correction d'erreur de vérification avec des chaînes alternatives">
<correction intel-microcode "Retour à des versions publiées précédemment de certains microcodes contournant des arrêts de l'initialisation sur Skylake-U/Y et Skylake Xeon E3">
<correction jackson-databind "Correction de multiples problèmes de sécurité affectant BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 et CVE-2019-17267]">
<correction jameica "Ajout de mckoisqldb au classpath, permettant l'utilisation du greffon SynTAX">
<correction jigdo "Correction de la prise en charge de HTTPS dans jigdo-lite et jigdo-mirror">
<correction ksh "Correction d'un problème de restriction de variable d'environnement [CVE-2019-14868]">
<correction lemonldap-ng "Correction d'une régression de la configuration de nginx introduite par le correctif pour le CVE-2019-19791">
<correction libapache-mod-jk "Fichier de configuration d'Apache renommé pour qu'il puisse être automatiquement activé et désactivé">
<correction libclamunrar "Nouvelle version amont stable ; ajout d'un méta-paquet non versionné">
<correction libembperl-perl "Gestion des pages d'erreur d'Apache &gt;= 2.4.40">
<correction libexif "Corrections de sécurité [CVE-2020-12767 CVE-2020-0093 CVE-2020-13112 CVE-2020-13113 CVE-2020-13114] ; correction de dépassement de tampon [CVE-2020-0182] et de dépassement d'entier [CVE-2020-0198]">
<correction libinput "Quirks : ajout de l'attribut d'intégration du <q>trackpoint</q>">
<correction libntlm "Correction de dépassement de tampon [CVE-2019-17455]">
<correction libpam-radius-auth "Correction de dépassement de tampon dans le champ du mot de passe [CVE-2015-9542]">
<correction libunwind "Correction d'erreurs de segmentation sur mips ; activation manuelle de la prise en charge d'exception C++ seulement sur i386 et amd64">
<correction libyang "Correction de plantage de corruption de cache, CVE-2019-19333, CVE-2019-19334">
<correction linux "Nouvelle version amont stable">
<correction linux-latest "Mise à jour pour l'ABI du noyau 4.19.0-10">
<correction linux-signed-amd64 "Nouvelle version amont stable">
<correction linux-signed-arm64 "Nouvelle version amont stable">
<correction linux-signed-i386 "Nouvelle version amont stable">
<correction lirc "Correction de la gestion de conffile">
<correction mailutils "maidag : abandon des privilèges setuid pour toutes les opérations de distribution sauf mda [CVE-2019-18862]">
<correction mariadb-10.3 "Nouvelle version amont stable ; corrections de sécurité [CVE-2020-2752 CVE-2020-2760 CVE-2020-2812 CVE-2020-2814 CVE-2020-13249] ; correction d'une régression dans la détection de RocksDB ZSTD">
<correction mod-gnutls "Correction d'une possible erreur de segmentation lors de l'échec d'une initialisation de connexion TLS ; correction des échecs de tests">
<correction multipath-tools "kpartx : utilisation du chemin correct vers partx dans la règle d'udev">
<correction mutt "Pas de vérification du chiffrement de IMAP PREAUTH si $tunnel est utilisé">
<correction mydumper "Lien vers libm">
<correction nfs-utils "statd : identité de l'utilisateur prise à partir de /var/lib/nfs/sm [CVE-2019-3689] ; /var/lib/nfs n'est plus rendue propriété de statd">
<correction nginx "Correction d'une vulnérabilité de dissimulation de requête de la page d'erreur [CVE-2019-20372]">
<correction nmap "Mise à jour de la taille de clé par défaut à 2048 bits">
<correction node-dot-prop "Correction d'une régression introduite dans le CVE-2020-8116 fix">
<correction node-handlebars "Interdiction d'un appel direct de <q>helperMissing</q> et <q>blockHelperMissing</q> [CVE-2019-19919]">
<correction node-minimist "Correction de pollution de prototype [CVE-2020-7598]">
<correction nvidia-graphics-drivers "Nouvelle version amont stable ; corrections de sécurité [CVE-2020-5963 CVE-2020-5967]">
<correction nvidia-graphics-drivers-legacy-390xx "Nouvelle version amont stable ; corrections de sécurité [CVE-2020-5963 CVE-2020-5967]">
<correction openstack-debian-images "Installation de resolvconf lors de l'installation de cloud-init">
<correction pagekite "Problèmes avec l'expiration des certificats SSL fournis évités en utilisant ceux du paquet ca-certificates">
<correction pdfchain "Correction d'un plantage au démarrage">
<correction perl "Correction de multiples problèmes de sécurité liés aux expressions rationnelles [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Correction d'une vulnérabilité de script intersite [CVE-2020-8035]">
<correction php-horde-gollem "Correction d'une vulnérabilité de script intersite dans la sortie de breadcrumb [CVE-2020-8034]">
<correction pillow "Correction de multiples problèmes de lectures hors limites [CVE-2020-11538 CVE-2020-10378 CVE-2020-10177]">
<correction policyd-rate-limit "Correction de problèmes de comptage dus à une réutilisation de socket">
<correction postfix "Nouvelle version amont stable ; correction d'une erreur de segmentation dans le rôle du client tlsproxy quand le rôle du serveur a été désactivé ; correction de <q>la valeur par défaut de maillog_file_rotate_suffix utilisait la minute à la place du mois</q> ; correction de plusieurs problèmes liés à TLS ; corrections du README.Debian">
<correction python-markdown2 "Correction d'un problème de script inter-site [CVE-2020-11888]">
<correction python3.7 "Boucle infinie évitée lors de la lecture de fichiers TAR contrefaits pour l'occasion en utilisant le module tarfile [CVE-2019-20907] ; collisions de hachage résolues pour IPv4Interface et IPv6Interface [CVE-2020-14422] ; correction d'un problème de déni de service dans urllib.request.AbstractBasicAuthHandler [CVE-2020-8492]">
<correction qdirstat "Correction de la sauvegarde des catégories MIME configurées par l'utilisateur">
<correction raspi3-firmware "Correction d'une coquille qui pourrait mener à des systèmes non amorçables">
<correction resource-agents "IPsrcaddr : <q>proto</q> rendu optionnel pour corriger une régression lors de son utilisation sans NetworkManager">
<correction ruby-json "Correction d'une vulnérabilité de création d'objet non sûr [CVE-2020-10663]">
<correction shim "Utilisation de clés de signature de Debian renouvelées après rotation">
<correction shim-helpers-amd64-signed "Utilisation de clés de signature de Debian renouvelées après rotation">
<correction shim-helpers-arm64-signed "Utilisation de clés de signature de Debian renouvelées après rotation">
<correction shim-helpers-i386-signed "Utilisation de clés de signature de Debian renouvelées après rotation">
<correction speedtest-cli "En-têtes corrects transmis pour corriger le test de rapidité de téléchargement ascendant">
<correction ssvnc "Correction d'écriture hors limites [CVE-2018-20020], de boucle infinie [CVE-2018-20021], d'initialisation incorrecte [CVE-2018-20022], d'un potentiel déni de service [CVE-2018-20024]">
<correction storebackup "Correction d'une possible vulnérabilité d'élévation de privilèges [CVE-2020-7040]">
<correction suricata "Correction d'abandon de privilèges dans nflog runmode">
<correction tigervnc "Pas d'utilisation de libunwind sur armel, armhf ou arm64">
<correction transmission "Correction d'un possible problème de déni de service [CVE-2018-10756]">
<correction wav2cdr "Utilisation de types d'entier à taille fixe de C99 pour corriger une assertion au moment de l'exécution sur les architectures 64 bits autres que amd64 et alpha">
<correction zipios++ "Correction de sécurité [CVE-2019-13453]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2020 4626 php7.3>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4679 keystone>
<dsa 2020 4680 tomcat9>
<dsa 2020 4681 webkit2gtk>
<dsa 2020 4682 squid>
<dsa 2020 4683 thunderbird>
<dsa 2020 4684 libreswan>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4690 dovecot>
<dsa 2020 4691 pdns-recursor>
<dsa 2020 4692 netqmail>
<dsa 2020 4694 unbound>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4696 nodejs>
<dsa 2020 4697 gnutls28>
<dsa 2020 4699 linux-signed-amd64>
<dsa 2020 4699 linux-signed-arm64>
<dsa 2020 4699 linux-signed-i386>
<dsa 2020 4699 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4707 mutt>
<dsa 2020 4708 neomutt>
<dsa 2020 4709 wordpress>
<dsa 2020 4710 trafficserver>
<dsa 2020 4711 coturn>
<dsa 2020 4712 imagemagick>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4714 chromium>
<dsa 2020 4716 docker.io>
<dsa 2020 4718 thunderbird>
<dsa 2020 4719 php7.3>
<dsa 2020 4720 roundcube>
<dsa 2020 4721 ruby2.5>
<dsa 2020 4722 ffmpeg>
<dsa 2020 4723 xen>
<dsa 2020 4724 webkit2gtk>
<dsa 2020 4725 evolution-data-server>
<dsa 2020 4726 nss>
<dsa 2020 4727 tomcat9>
<dsa 2020 4728 qemu>
<dsa 2020 4729 libopenmpt>
<dsa 2020 4730 ruby-sanitize>
<dsa 2020 4731 redis>
<dsa 2020 4732 squid>
<dsa 2020 4733 qemu>
<dsa 2020 4735 grub-efi-amd64-signed>
<dsa 2020 4735 grub-efi-arm64-signed>
<dsa 2020 4735 grub-efi-ia32-signed>
<dsa 2020 4735 grub2>
</table>


<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction golang-github-unknwon-cae "Problèmes de sécurité ; non maintenu">
<correction janus "Pas de prise en charge dans stable">
<correction mathematica-fonts "S'appuie sur des emplacements de téléchargement indisponibles">
<correction matrix-synapse "Problèmes de sécurité ; non pris en charge">
<correction selenium-firefoxdriver "Incompatible avec les dernières versions de Firefox ESR">

</table>

<h2>Installateur Debian</h2>
<p>L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de stable.</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>


<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution stable :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
