#use wml::debian::template title="Getting Debian"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Debian is distributed <a href="../intro/free">freely</a>
over Internet. You can download all of it from any of our
<a href="ftplist">mirrors</a>.
The <a href="../releases/stable/installmanual">Installation Manual</a>
contains detailed installation instructions.
And, the release notes can be found <a href="../releases/stable/releasenotes">here</a>.
</p>

<p>This page has options for installing Debian Stable. If you are interested in Testing
   or Unstable, visit our <a href="../releases/">releases page</a>.</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Download an installation image</a></h2>
    <p>Depending on your Internet connection, you may download either of the following:</p>
    <ul>
      <li>A <a href="netinst"><strong>small installation image</strong></a>:
	    can be downloaded quickly and should be recorded onto a removable
	    disk. To use this, you will need a machine with an Internet
	    connection.
	<ul class="quicklist downlist">
	  <li><a title="Download installer for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bit
	      PC netinst iso</a></li>
	  <li><a title="Download installer for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bit
	      PC netinst iso</a></li>
	</ul>
      </li>
      <li>A larger <a href="../CD/"><strong>complete installation
	image</strong></a>: contains more packages, making it easier to install
	machines without an Internet connection.
	<ul class="quicklist downlist">
	  <li><a title="Download DVD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-dvd/">64-bit PC torrents (DVD)</a></li>
	  <li><a title="Download DVD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-dvd/">32-bit PC torrents (DVD)</a></li>
	  <li><a title="Download CD torrents for 64-bit Intel and AMD PC"
	         href="<stable-images-url/>/amd64/bt-cd/">64-bit PC torrents (CD)</a></li>
	  <li><a title="Download CD torrents for normal 32-bit Intel and AMD PC"
		 href="<stable-images-url/>/i386/bt-cd/">32-bit PC torrents (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Use a Debian cloud image</a></h2>
    <ul>
      <li>An official <a href="https://cloud.debian.org/images/cloud/"><strong>cloud image</strong></a>:
            can be used directly on your cloud provider, built by the Debian Cloud Team.
        <ul class="quicklist downlist">
          <li><a title="OpenStack image for 64-bit Intel and AMD Qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2">64-bit AMD/Intel OpenStack (Qcow2)</a></li>
          <li><a title="OpenStack image for 64-bit ARM Qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.qcow2">64-bit ARM OpenStack (Qcow2)</a></li>
        </ul>
      </li>
    </ul>
    <h2><a href="../CD/live/">Try Debian live before installing</a></h2>
    <p>
      You can try Debian by booting a live system from a CD, DVD or USB key
      without installing any files to the computer. When you are ready, you can
      run the included installer (starting from Debian 10 Buster, this is the
      end-user-friendly <a href="https://calamares.io">Calamares Installer</a>).
      Provided the images meet your size, language,
      and package selection requirements, this method may be suitable for you.
      Read more <a href="../CD/live#choose_live">information about this method</a>
      to help you decide.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Download live torrents for 64-bit Intel and AMD PC"
	     href="<live-images-url/>/amd64/bt-hybrid/">64-bit PC live torrents</a></li>
      <li><a title="Download live torrents for normal 32-bit Intel and AMD PC"
	     href="<live-images-url/>/i386/bt-hybrid/">32-bit PC live torrents</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Buy a set of CDs or DVDs from one of the
      vendors selling Debian CDs</a></h2>

   <p>
      Many of the vendors sell the distribution for less than US$5 plus
      shipping (check their web page to see if they ship internationally).
      <br />
      Some of the <a href="../doc/books">books about Debian</a> come with
      CDs, too.
   </p>

   <p>Here are the basic advantages of CDs:</p>

   <ul>
     <li>Installation from a CD set is more straightforward.</li>
     <li>You can install on machines without an Internet connection.</li>
	 <li>You can install Debian (on as many machines as you like) without downloading
	 all packages yourself.</li>
     <li>The CD can be used to more easily rescue a damaged Debian system.</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="pre-installed">Buy a computer with Debian
      pre-installed</a></h2>
   <p>There are a number of advantages to this:</p>
   <ul>
    <li>You don't have to install Debian.</li>
    <li>The installation is pre-configured to match the hardware.</li>
    <li>The vendor may provide technical support.</li>
   </ul>
  </div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
If any of the hardware in your system <strong>requires non-free firmware to be
loaded</strong> with the device driver, you can use one of the
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
tarballs of common firmware packages</a> or download an <strong>unofficial</strong> image
including these <strong>non-free</strong> firmwares. Instructions how to use the tarballs
and general information about loading firmware during an installation can
be found in the <a href="../releases/stable/amd64/ch06s04">Installation Guide</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">unofficial
installation images for <q>stable</q> with firmware included</a>
</p>
</div>
