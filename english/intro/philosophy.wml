#use wml::debian::template title="Our philosophy: why we do it and how we do it"
#include "$(ENGLISHDIR)/releases/info"

# translators: some text is taken from /intro/about.wml

<ul class="toc">
<li><a href="#what">WHAT is Debian?</a>
<li><a href="#free">It's all free?</a>
<li><a href="#how">How does the community work as a project?</a>
<li><a href="#history">How'd it all get started?</a>
</ul>

<h2><a name="what">WHAT is Debian?</a></h2>

<p>The <a href="$(HOME)/">Debian Project</a> is an association of
individuals who have made common cause to create a <a href="free">free</a>
operating system. This operating system that we have created is called
<strong>Debian</strong>.

<p>An operating system is the set of basic programs and utilities that make
your computer run.
At the core of an operating system is the kernel.
The kernel is the most fundamental program on the computer and does all the basic
housekeeping and lets you start other programs.

<p>Debian systems currently use the <a href="https://www.kernel.org/">Linux</a>
kernel or the <a href="https://www.freebsd.org/">FreeBSD</a>
kernel. Linux is a piece of software started by 
<a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a>
and supported by thousands of programmers worldwide.
FreeBSD is an operating system including a kernel and other software.

<p>However, work is in progress to provide Debian for other kernels,
primarily for
<a href="https://www.gnu.org/software/hurd/hurd.html">the Hurd</a>.
The Hurd is a collection of servers that run on top of a microkernel (such as
Mach) to implement different features. The Hurd is free software produced by the
<a href="https://www.gnu.org/">GNU project</a>.

<p>A large part of the basic tools that fill out the operating system come
from the <a href="https://www.gnu.org/">GNU project</a>; hence the names:
GNU/Linux, GNU/kFreeBSD, and GNU/Hurd.
These tools are also free.

<p>Of course, the thing that people want is application software: programs
to help them get what they want to do done, from editing documents to
running a business to playing games to writing more software. Debian comes
with over <packages_in_stable> <a href="$(DISTRIB)/packages">packages</a> (precompiled
software that is bundled up in a nice format for easy installation on your
machine), a package manager (APT), and other utilities that make it possible
to manage thousands of packages on thousands of computers as easily as
installing a single application. All of it <a href="free">free</a>.
</p>

<p>It's a bit like a tower. At the base is the kernel.
On top of that are all the basic tools.
Next is all the software that you run on the computer.
At the top of the tower is Debian &mdash; carefully organizing and fitting
everything so it all works together.

<h2>It's all <a href="free" name="free">free?</a></h2>

<p>When we use the word "free", we are referring to software
<strong>freedom</strong>. You can read more on
<a href="free">what we mean by "free software"</a> and
<a href="https://www.gnu.org/philosophy/free-sw">what the Free Software
Foundation says</a> on that subject.

<p>You may be wondering: why would people spend hours of their own time to write
software, carefully package it, and then <EM>give</EM> it all away?
The answers are as varied as the people who contribute.
Some people like to help others.
Many write programs to learn more about computers.
More and more people are looking for ways to avoid the inflated price of
software.
A growing crowd contribute as a thank you for all the great free software they've
received from others.
Many in academia create free software to help get the results of
their research into wider use.
Businesses help maintain free software so they can have a say in how it develops --
there's no quicker way to get a new feature than to implement it yourself!
Of course, a lot of us just find it great fun.

<p>Debian is so committed to free software that we thought it would be useful if that
commitment was formalized in a written document. Thus, our
<a href="$(HOME)/social_contract">Social Contract</a> was born.

<p>Although Debian believes in free software, there are cases where people want or need to
put non-free software on their machine. Whenever possible Debian will support this.
There are even a growing number of packages whose sole job is to install non-free software
into a Debian system.

<h2><a name="how">How does the community work as a project?</a></h2>

<p>Debian is produced by almost a thousand active
developers spread
<a href="$(DEVEL)/developers.loc">around the world</a> who volunteer
in their spare time.
Few of the developers have actually met in person.
Communication is done primarily through e-mail (mailing lists at
lists.debian.org) and IRC (#debian channel at irc.debian.org).
</p>

<p>The Debian Project has a carefully <a href="organization">organized
structure</a>. For more information on how Debian looks from the inside,
please feel free to browse the <a href="$(DEVEL)/">developers' corner</a>.</p>

<p>
The main documents explaining how the community works are the following:
<ul>
<li><a href="$(DEVEL)/constitution">The Debian Constitution</a></li>
<li><a href="../social_contract">The Social Contract and the Free Software Guidelines</a></li>
<li><a href="diversity">The Diversity Statement</a></li>
<li><a href="../code_of_conduct">The Code of Conduct</a></li>
<li><a href="../doc/developers-reference/">The Developer's Reference</a></li>
<li><a href="../doc/debian-policy/">The Debian Policy</a></li>
</ul>

<h2><a name="history">How'd it all get started?</a></h2>

<p>Debian was begun in August 1993 by Ian Murdock, as a new distribution
which would be made openly, in the spirit of Linux and GNU. Debian was meant
to be carefully and conscientiously put together, and to be maintained and
supported with similar care. It started as a small, tightly-knit group of
Free Software hackers, and gradually grew to become a large, well-organized
community of developers and users. See
<a href="$(DOC)/manuals/project-history/">the detailed history</a>.

<p>Since many people have asked, Debian is pronounced /&#712;de.bi.&#601;n/. It
comes from the names of the creator of Debian, Ian Murdock, and his wife,
Debra.
