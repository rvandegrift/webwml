<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerability has been discovered in the webkit2gtk web
engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3885">CVE-2020-3885</a>

    <p>Ryan Pickren discovered that a file URL may be incorrectly
    processed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3894">CVE-2020-3894</a>

    <p>Sergei Glazunov discovered that a race condition may allow an
    application to read restricted memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3895">CVE-2020-3895</a>

    <p>grigoritchy discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3897">CVE-2020-3897</a>

    <p>Brendan Draper discovered that a remote attacker may be able to
    cause arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3899">CVE-2020-3899</a>

    <p>OSS-Fuzz discovered that a remote attacker may be able to cause
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3900">CVE-2020-3900</a>

    <p>Dongzhuo Zhao discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3901">CVE-2020-3901</a>

    <p>Benjamin Randazzo discovered that processing maliciously crafted
    web content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3902">CVE-2020-3902</a>

    <p>Yigit Can Yilmaz discovered that processing maliciously crafted
    web content may lead to a cross site scripting attack.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.28.2-2~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4681.data"
# $Id: $
