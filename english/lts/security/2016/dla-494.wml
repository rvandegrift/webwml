<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been fixed in the Debian GNU C Library,
eglibc:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1234">CVE-2016-1234</a>

    <p>Alexander Cherepanov discovered that the glibc's glob implementation
    suffered from a stack-based buffer overflow when it was called with the
    GLOB_ALTDIRFUNC flag and encountered a long file name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3075">CVE-2016-3075</a>

    <p>The getnetbyname implementation in nss_dns was susceptible to a stack
    overflow and a crash if it was invoked on a very long name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3706">CVE-2016-3706</a>

    <p>Michael Petlan reported that getaddrinfo copied large amounts of address
    data to the stack, possibly leading to a stack overflow. This complements
    the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2013-4458">CVE-2013-4458</a>.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.13-38+deb7u11.</p>

<p>We recommend you to upgrade your eglibc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system, and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-494.data"
# $Id: $
