<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>"unzip -l" (<a href="https://security-tracker.debian.org/tracker/CVE-2014-9913">CVE-2014-9913</a>) and <q>zipinfo</q> (<a href="https://security-tracker.debian.org/tracker/CVE-2016-9844">CVE-2016-9844</a>) were vulnerable
to buffer overflows when provided malformed or maliciously-crafted ZIP
files.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
6.0-8+deb7u6.</p>

<p>We recommend that you upgrade your unzip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-741.data"
# $Id: $
