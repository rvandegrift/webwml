<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in jupyter-notebook.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8768">CVE-2018-8768</a>

    <p>A maliciously forged notebook file can bypass sanitization to execute
    Javascript in the notebook context. Specifically, invalid HTML is
    <q>fixed</q> by jQuery after sanitization, making it dangerous.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19351">CVE-2018-19351</a>

    <p>allows XSS via an untrusted notebook because nbconvert responses are
    considered to have the same origin as the notebook server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-21030">CVE-2018-21030</a>

    <p>jupyter-notebook does not use a CSP header to treat served files as
    belonging to a separate origin. Thus, for example, an XSS payload can
    be placed in an SVG document.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.2.3-4+deb9u1.</p>

<p>We recommend that you upgrade your jupyter-notebook packages.</p>

<p>For the detailed security status of jupyter-notebook please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jupyter-notebook">https://security-tracker.debian.org/tracker/jupyter-notebook</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2432.data"
# $Id: $
