<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in Drupal, a fully-featured content
management framework.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13666">CVE-2020-13666</a>

    <p>The Drupal AJAX API did not disable JSONP by default, which could
    lead to cross-site scripting.</p>

    <p>For setups that relied on Drupal's AJAX API for JSONP requests,
    either JSONP will need to be reenabled, or the jQuery AJAX API will
    have to be used instead.</p>

    <p>See the upstream advisory for more details:
    <a href="https://www.drupal.org/sa-core-2020-007">https://www.drupal.org/sa-core-2020-007</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13671">CVE-2020-13671</a>

    <p>Drupal failed to sanitize filenames on uploaded files, which could
    lead to those files being served as the wrong MIME type, or being
    executed depending on the server configuration.</p>

    <p>It is also recommended to check previously uploaded files for
    malicious extensions. For more details see the upstream advisory:
    <a href="https://www.drupal.org/sa-core-2020-012">https://www.drupal.org/sa-core-2020-012</a></p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
7.52-2+deb9u12.</p>

<p>We recommend that you upgrade your drupal7 packages.</p>

<p>For the detailed security status of drupal7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/drupal7">https://security-tracker.debian.org/tracker/drupal7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2458.data"
# $Id: $
