<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in Zabbix, a network
monitoring solution. An attacker may remotely execute code on the
zabbix server, and redirect to external links through the zabbix web
frontend.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10742">CVE-2016-10742</a>

    <p>Zabbix allows open redirect via the request parameter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11800">CVE-2020-11800</a>

    <p>Zabbix allows remote attackers to execute arbitrary code.</p></li>

</ul>

<p>This update also includes several other bug fixes and
improvements. For more information please refer to the upstream
changelog file.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1:3.0.31+dfsg-0+deb9u1.</p>

<p>We recommend that you upgrade your zabbix packages.</p>

<p>For the detailed security status of zabbix please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zabbix">https://security-tracker.debian.org/tracker/zabbix</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2461.data"
# $Id: $
