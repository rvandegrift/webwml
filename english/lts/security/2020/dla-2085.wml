<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in zlib, a compression library.
They are basically about improper big-endian CRC calculation, improper
left shift of negative integers and improper pointer arithmetic.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:1.2.8.dfsg-2+deb8u1.</p>

<p>We recommend that you upgrade your zlib packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2085.data"
# $Id: $
