<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>John Hawthorn of Github discovered a file content disclosure
vulnerability in Rails, a ruby based web application framework.
Specially crafted accept headers in combination with calls to `render
file:` can cause arbitrary files on the target server to be rendered,
disclosing the file contents.</p>

<p>This vulnerability could also be exploited for a denial-of-service
attack.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:4.1.8-1+deb8u5.</p>

<p>We recommend that you upgrade your rails packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1739.data"
# $Id: $
