<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Samuel R Lovejoy discovered a security vulnerability in dnsmasq.
Carefully crafted packets by DNS servers might result in out of
bounds read operations, potentially leading to a crash and denial
of service.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.72-3+deb8u5.</p>

<p>We recommend that you upgrade your dnsmasq packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1921.data"
# $Id: $
