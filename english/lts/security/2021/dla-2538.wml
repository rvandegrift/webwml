<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were fixed by upgrading the MariaDB database server
packages to the latest version on the 10.1 branch.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
10.1.48-0+deb9u1.</p>

<p>We recommend that you upgrade your mariadb-10.1 packages.</p>

<p>For the detailed security status of mariadb-10.1 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mariadb-10.1">https://security-tracker.debian.org/tracker/mariadb-10.1</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2538.data"
# $Id: $
