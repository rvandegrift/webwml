<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were found in ImageMagick, a suite of
image manipulation programs. An attacker could cause denial of service
and execution of arbitrary code when a crafted image file is
processed.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14528">CVE-2017-14528</a>

    <p>The TIFFSetProfiles function in coders/tiff.c has incorrect
    expectations about whether LibTIFF TIFFGetField return values
    imply that data validation has occurred, which allows remote
    attackers to cause a denial of service (use-after-free after an
    invalid call to TIFFSetField, and application crash) via a crafted
    file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19667">CVE-2020-19667</a>

    <p>Stack-based buffer overflow and unconditional jump in ReadXPMImage
    in coders/xpm.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25665">CVE-2020-25665</a>

    <p>The PALM image coder at coders/palm.c makes an improper call to
    AcquireQuantumMemory() in routine WritePALMImage() because it
    needs to be offset by 256. This can cause a out-of-bounds read
    later on in the routine. This could cause impact to reliability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25674">CVE-2020-25674</a>

    <p>WriteOnePNGImage() from coders/png.c (the PNG coder) has a for
    loop with an improper exit condition that can allow an
    out-of-bounds READ via heap-buffer-overflow. This occurs because
    it is possible for the colormap to have less than 256 valid values
    but the loop condition will loop 256 times, attempting to pass
    invalid colormap data to the event logger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27560">CVE-2020-27560</a>

    <p>ImageMagick allows Division by Zero in OptimizeLayerFrames in
    MagickCore/layer.c, which may cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27750">CVE-2020-27750</a>

    <p>A flaw was found in MagickCore/colorspace-private.h and
    MagickCore/quantum.h. An attacker who submits a crafted file that
    is processedcould trigger undefined behavior in the form of values
    outside the range of type `unsigned char` and math division by
    zero. This would most likely lead to an impact to application
    availability, but could potentially cause other problems related
    to undefined behavior.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27760">CVE-2020-27760</a>

    <p>In `GammaImage()` of /MagickCore/enhance.c, depending on the
    `gamma` value, it's possible to trigger a divide-by-zero condition
    when a crafted input file is processed by ImageMagick. This could
    lead to an impact to application availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27763">CVE-2020-27763</a>

    <p>A flaw was found in MagickCore/resize.c. An attacker who submits a
    crafted file that is processed by ImageMagick could trigger
    undefined behavior in the form of math division by zero. This
    would most likely lead to an impact to application availability,
    but could potentially cause other problems related to undefined
    behavior.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27765">CVE-2020-27765</a>

    <p>A flaw was found in MagickCore/segment.c. An attacker who submits
    a crafted file that is processed by ImageMagick could trigger
    undefined behavior in the form of math division by zero. This
    would most likely lead to an impact to application availability,
    but could potentially cause other problems related to undefined
    behavior.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27773">CVE-2020-27773</a>

    <p>A flaw was found in MagickCore/gem-private.h. An attacker who
    submits a crafted file that is processed by ImageMagick could
    trigger undefined behavior in the form of values outside the range
    of type `unsigned char` or division by zero. This would most
    likely lead to an impact to application availability, but could
    potentially cause other problems related to undefined behavior.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29599">CVE-2020-29599</a>

    <p>ImageMagick mishandles the -authenticate option, which allows
    setting a password for password-protected PDF files. The
    user-controlled password was not properly escaped/sanitized and it
    was therefore possible to inject additional shell commands via
    coders/pdf.c.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
8:6.9.7.4+dfsg-11+deb9u11.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>For the detailed security status of imagemagick please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/imagemagick">https://security-tracker.debian.org/tracker/imagemagick</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2523.data"
# $Id: $
