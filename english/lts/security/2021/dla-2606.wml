<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in lxml, a pythonic binding for the libxml2 and
libxslt libraries.</p>

<p>Due to missing input sanitization, XSS is possible for the HTML5
formaction attribute.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
3.7.1-1+deb9u4.</p>

<p>We recommend that you upgrade your lxml packages.</p>

<p>For the detailed security status of lxml please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lxml">https://security-tracker.debian.org/tracker/lxml</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2606.data"
# $Id: $
