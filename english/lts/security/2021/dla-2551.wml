<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in slirp, a SLIP/PPP emulator using a dial up
shell account.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7039">CVE-2020-7039</a>

     <p>Due to mismanagement of memory, a heap-based buffer overflow or
     other out-of-bounds access might happen, which can lead to a DoS
     or potential execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8608">CVE-2020-8608</a>

     <p>Prevent a buffer overflow vulnerability due to incorrect usage
     of return values from snprintf.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1:1.0.17-8+deb9u1.</p>

<p>We recommend that you upgrade your slirp packages.</p>

<p>For the detailed security status of slirp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/slirp">https://security-tracker.debian.org/tracker/slirp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2551.data"
# $Id: $
