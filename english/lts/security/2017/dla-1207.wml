<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An erlang TLS server configured with cipher suites using RSA key exchange,
may be vulnerable to an Adaptive Chosen Ciphertext attack (AKA
Bleichenbacher attack) against RSA, which when exploited, may result in
plaintext recovery of encrypted messages and/or a Man-in-the-middle (MiTM)
attack, despite the attacker not having gained access to the server's
private key itself.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
15.b.1-dfsg-4+deb7u2.</p>

<p>We recommend that you upgrade your erlang packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1207.data"
# $Id: $
