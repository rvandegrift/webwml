<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Some vulnerabilities were found in the Ruby 1.8 package that affects
the LTS distribution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0898">CVE-2017-0898</a>

    <p>Buffer underrun vulnerability in Kernel.sprintf</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10784">CVE-2017-10784</a>

    <p>Escape sequence injection vulnerability in the Basic
    authentication of WEBrick</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.8.7.358-7.1+deb7u4.</p>

<p>We recommend that you upgrade your ruby1.8 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1113.data"
# $Id: $
