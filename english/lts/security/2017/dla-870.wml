<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>More vulnerabilities were discovered in libplist, a library for
reading and writing the Apple binary and XML property lists format.
A maliciously crafted plist file could cause a denial-of-service
(application crash) by triggering a heap-based buffer overflow or
memory allocation error in the parse_string_node function.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.8-1+deb7u3.</p>

<p>We recommend that you upgrade your libplist packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-870.data"
# $Id: $
