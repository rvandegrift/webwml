<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two significant vulnerabilities were found in the Mercurial version
control system which could lead to shell injection attacks and
out-of-tree file overwrite.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000115">CVE-2017-1000115</a>

    <p>Mercurial's symlink auditing was incomplete prior to 4.3, and
    could be abused to write to files outside the repository.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000116">CVE-2017-1000116</a>

    <p>Mercurial was not sanitizing hostnames passed to ssh, allowing
    shell injection attacks on clients by specifying a hostname
    starting with -oProxyCommand. This vulnerability is similar to
    those in Git (<a href="https://security-tracker.debian.org/tracker/CVE-2017-1000117">CVE-2017-1000117</a>) and Subversion 
    (<a href="https://security-tracker.debian.org/tracker/CVE-2017-9800">CVE-2017-9800</a>).</p>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.2.2-4+deb7u5.</p>

<p>We recommend that you upgrade your mercurial packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1072.data"
# $Id: $
