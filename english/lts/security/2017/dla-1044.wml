<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The racoon daemon in IPsec-Tools 0.8.2 and earlier contains a remotely
exploitable computational-complexity attack when parsing and storing
ISAKMP fragments. The implementation permits a remote attacker to
exhaust computational resources on the remote endpoint by repeatedly
sending ISAKMP fragment packets in a particular order such that the
worst-case computational complexity is realized in the algorithm
utilized to determine if reassembly of the fragments can take place.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:0.8.0-14+deb7u2.</p>

<p>We recommend that you upgrade your ipsec-tools packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1044.data"
# $Id: $
