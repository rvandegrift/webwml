#use wml::debian::translation-check translation="6547bb7720bfba1c2481b95c44642a4a6d3df030"
<define-tag pagetitle>Debian 10 更新：10.4 发布</define-tag>
<define-tag release_date>2020-05-09</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian 项目很高兴地宣布对 Debian <release> 稳定版的第四次更新（发行版代号 \
<q><codename></q>）。此次小版本更新主要添加了对安全问题的修正补丁，以及为一些严重问题\
所作的调整。安全通告已单独发布，并会在适当的情况下予以引用。</p>

<p>请注意，此更新并不是 Debian <release> 的新版本，它仅更新了所包含的一些软件包。\
没有必要丢弃旧的<q><codename></q>的安装介质。在安装之后，只需使用最新的 Debian \
镜像更新旧的软件包即可。</p>

<p>经常从 security.debian.org 安装更新的用户将不必更新许多软件包，\
因本更新中包含了 security.debian.org 的大多数更新。</p>

<p>新的安装镜像即将于常规的位置予以提供。</p>

<p>只需令软件包管理系统指向 Debian 的许多 HTTP 镜像站点之一，\
您便能够把已有的系统升级至本次更新版本。详尽的镜像列表可以在以下网址处获得：</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>杂项错误修正</h2>

<p>此稳定版更新为以下软件包添加了一些重要的修正：</p>

<table border=0>
<tr><th>软件包</th>               <th>原因</th></tr>
<correction apt-cacher-ng "Enforce secured call to the server in maintenance job triggering [CVE-2020-5202]; allow .zst compression for tarballs; increase size of the decompression line buffer for configuration file reading">
<correction backuppc "Pass the username to start-stop-daemon when reloading, preventing reload failures">
<correction base-files "为小版本更新提供文件">
<correction brltty "Reduce severity of log message to avoid generating too many messages when used with new Orca versions">
<correction checkstyle "Fix XML External Entity injection issue [CVE-2019-9658 CVE-2019-10782]">
<correction choose-mirror "更新其包含的镜像列表">
<correction clamav "新上游发行版本 [CVE-2020-3123]">
<correction corosync "totemsrp: Reduce MTU to avoid generating oversized packets">
<correction corosync-qdevice "修复服务启动问题">
<correction csync2 "Fail HELLO command when SSL is required">
<correction cups "Fix heap buffer overflow [CVE-2020-3898] and <q>the `ippReadIO` function may under-read an extension field</q> [CVE-2019-8842]">
<correction dav4tbsync "新上游发行版本，修复与新版本 Thunderbird 的兼容性">
<correction debian-edu-config "Add policy files for Firefox ESR and Thunderbird to fix the TLS/SSL setup">
<correction debian-installer "为 4.19.0-9 kernel ABI 更新">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-security-support "新上游稳定释出版本；更新几个软件包的状态；改用 <q>runuser</q> 而不是 <q>su</q>">
<correction distro-info-data "添加 Ubuntu 20.10 以及 stretch 的可能结束支持日期">
<correction dojo "Fix improper regular expression usage [CVE-2019-10785]">
<correction dpdk "新上游稳定释出版本">
<correction dtv-scan-tables "New upstream snapshot; add all current German DVB-T2 muxes and the Eutelsat-5-West-A satellite">
<correction eas4tbsync "新上游发行版本，修复与新版本 Thunderbird 的兼容性">
<correction edk2 "安全修复 [CVE-2019-14558 CVE-2019-14559 CVE-2019-14563 CVE-2019-14575 CVE-2019-14586 CVE-2019-14587]">
<correction el-api "修复从 stretch 升级到 buster 时包含 Tomcat 8 的问题">
<correction fex "Fix a potential security issue in fexsrv">
<correction filezilla "Fix untrusted search path vulnerability [CVE-2019-5429]">
<correction frr "Fix extended next hop capability">
<correction fuse "Remove outdated udevadm commands from post-install scripts; don't explicitly remove fuse.conf on purge">
<correction fuse3 "Remove outdated udevadm commands from post-install scripts; don't explicitly remove fuse.conf on purge; fix memory leak in fuse_session_new()">
<correction golang-github-prometheus-common "Extend validity of test certificates">
<correction gosa "Replace (un)serialize with json_encode/json_decode to mitigate PHP object injection [CVE-2019-14466]">
<correction hbci4java "Support EU directive on payment services (PSD2)">
<correction hibiscus "Support EU directive on payment services (PSD2)">
<correction iputils "Correct an issue in which ping would improperly exit with a failure code when there were untried addresses still available in the getaddrinfo() library call return value">
<correction ircd-hybrid "Use dhparam.pem to avoid crash on startup">
<correction jekyll "允许使用 ruby-i18n 0.x 和 1.x">
<correction jsp-api "修复从 stretch 升级到 buster 时包含 Tomcat 8 的问题">
<correction lemonldap-ng "Prevent unwanted access to administration endpoints [CVE-2019-19791]; fix the GrantSession plugin which could not prohibit logon when two factor authentication was used; fix arbitrary redirects with OIDC if redirect_uri was not used">
<correction libdatetime-timezone-perl "Update included data">
<correction libreoffice "Fix OpenGL slide transitions">
<correction libssh "Fix possible denial of service issue when handling AES-CTR keys with OpenSSL [CVE-2020-1730]">
<correction libvncserver "修复堆溢出 [CVE-2019-15690]">
<correction linux "新上游稳定释出版本">
<correction linux-latest "更新 kernel ABI 到 4.19.0-9">
<correction linux-signed-amd64 "新上游稳定释出版本">
<correction linux-signed-arm64 "新上游稳定释出版本">
<correction linux-signed-i386 "新上游稳定释出版本">
<correction lwip "修复缓冲区溢出 [CVE-2020-8597]">
<correction lxc-templates "新上游稳定释出版本; handle languages that are only UTF-8 encoded">
<correction manila "Fix missing access permissions check [CVE-2020-9543]">
<correction megatools "添加对 mega.nz 链接的新格式的支持">
<correction mew "Fix server SSL certificate validity checking">
<correction mew-beta "Fix server SSL certificate validity checking">
<correction mkvtoolnix "Rebuild to tighten libmatroska6v5 dependency">
<correction ncbi-blast+ "禁用对 SSE4.2 的支持">
<correction node-anymatch "移除不必要的依赖">
<correction node-dot "Prevent code execution after prototype pollution [CVE-2020-8141]">
<correction node-dot-prop "Fix prototype pollution [CVE-2020-8116]">
<correction node-knockout "Fix escaping with older Internet Explorer versions [CVE-2019-14862]">
<correction node-mongodb "Reject invalid _bsontypes [CVE-2019-2391 CVE-2020-7610]">
<correction node-yargs-parser "Fix prototype pollution [CVE-2020-7608]">
<correction npm "Fix arbitrary path access [CVE-2019-16775 CVE-2019-16776 CVE-2019-16777]">
<correction nvidia-graphics-drivers "新上游稳定释出版本">
<correction nvidia-graphics-drivers-legacy-390xx "新上游稳定释出版本">
<correction nvidia-settings-legacy-340xx "新上游发行版本">
<correction oar "Revert to stretch behavior for Storable::dclone perl function, fixing recursion depth issues">
<correction opam "Prefer mccs over aspcud">
<correction openvswitch "Fix vswitchd abort when a port is added and the controller is down">
<correction orocos-kdl "Fix string conversion with Python 3">
<correction owfs "Remove broken Python 3 packages">
<correction pango1.0 "Fix crash in pango_fc_font_key_get_variations() when key is null">
<correction pgcli "Add missing dependency on python3-pkg-resources">
<correction php-horde-data "Fix authenticated remote code execution vulnerability [CVE-2020-8518]">
<correction php-horde-form "Fix authenticated remote code execution vulnerability [CVE-2020-8866]">
<correction php-horde-trean "Fix authenticated remote code execution vulnerability [CVE-2020-8865]">
<correction postfix "新上游稳定释出版本； fix panic with Postfix multi-Milter configuration during MAIL FROM; fix d/init.d running change so it works with multi-instance again">
<correction proftpd-dfsg "Fix memory access issue in keyboard-interative code in mod_sftp; properly handle DEBUG, IGNORE, DISCONNECT, and UNIMPLEMENTED messages in keyboard-interactive mode">
<correction puma "Fix Denial of Service issue [CVE-2019-16770]">
<correction purple-discord "Fix crashes in ssl_nss_read">
<correction python-oslo.utils "Fix leak of sensitive information via mistral logs [CVE-2019-3866]">
<correction rails "Fix possible cross-site scripting via Javascript escape helper [CVE-2020-5267]">
<correction rake "Fix command injection vulnerability [CVE-2020-8130]">
<correction raspi3-firmware "Fix dtb names mismatch in z50-raspi-firmware; fix boot on Raspberry Pi families 1 and 0">
<correction resource-agents "Fix <q>ethmonitor does not list interfaces without assigned IP address</q>; remove no longer required xen-toolstack patch; fix non-standard usage in ZFS agent">
<correction rootskel "Disable multiple console support if preseeding is in use">
<correction ruby-i18n "Fix gemspec generation">
<correction rubygems-integration "Avoid deprecation warnings when users install a newer version of Rubygems via <q>gem update --system</q>">
<correction schleuder "Improve patch to handle encoding errors introduced in the previous version; switch default encoding to UTF-8; let x-add-key handle mails with attached, quoted-printable encoded keys; fix x-attach-listkey with mails created by Thunderbird that include protected headers">
<correction scilab "Fix library loading with OpenJDK 11.0.7">
<correction serverspec-runner "支持 Ruby 2.5">
<correction softflowd "Fix broken flow aggregation which might result in flow table overflow and 100% CPU usage">
<correction speech-dispatcher "Fix default pulseaudio latency which triggers <q>scratchy</q> output">
<correction spl-linux "修复死锁">
<correction sssd "Fix sssd_be busy-looping when LDAP connection is intermittent">
<correction systemd "when authorizing via PolicyKit re-resolve callback/userdata instead of caching it [CVE-2020-1712]; install 60-block.rules in udev-udeb and initramfs-tools">
<correction taglib "Fix corruption issues with OGG files">
<correction tbsync "新上游发行版本，修复与新版本 Thunderbird 的兼容性">
<correction timeshift "Fix predictable temporary directory use [CVE-2020-10174]">
<correction tinyproxy "Only set PIDDIR, if PIDFILE is a non-zero length string">
<correction tzdata "新上游稳定释出版本">
<correction uim "unregister modules that are not installed, fixing a regression in the previous upload">
<correction user-mode-linux "Fix build failure with current stable kernels">
<correction vite "Fix crash when there are more than 32 elements">
<correction waagent "新上游发行版本；支持与 cloud-init 共同安装">
<correction websocket-api "修复从 stretch 升级到 buster 时包含 Tomcat 8 的问题">
<correction wpa "Do not try to detect PSK mismatch during PTK rekeying; check for FT support when selecting FT suites; fix MAC randomisation issue with some cards">
<correction xdg-utils "xdg-open: fix pcmanfm check and handling of directories with spaces in their names; xdg-screensaver: Sanitise window name before sending it over D-Bus; xdg-mime: Create config directory if it does not exist yet">
<correction xtrlock "Fix blocking of (some) multitouch devices while locked [CVE-2016-10894]">
<correction zfs-linux "修复潜在的死锁问题">
</table>


<h2>安全更新</h2>


<p>此修订版本将以下安全更新添加到了稳定发行版本中。安全团队已经分别为这些更新发布了通告：</p>

<table border=0>
<tr><th>通告编号</th>  <th>软件包</th></tr>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4623 postgresql-11>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4627 webkit2gtk>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4636 python-bleach>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4638 chromium>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4641 webkit2gtk>
<dsa 2020 4642 thunderbird>
<dsa 2020 4643 python-bleach>
<dsa 2020 4644 tor>
<dsa 2020 4645 chromium>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4649 haproxy>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4651 mediawiki>
<dsa 2020 4652 gnutls28>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4654 chromium>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4658 webkit2gtk>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4661 openssl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4665 qemu>
<dsa 2020 4666 openldap>
<dsa 2020 4667 linux-signed-amd64>
<dsa 2020 4667 linux-signed-arm64>
<dsa 2020 4667 linux-signed-i386>
<dsa 2020 4667 linux>
<dsa 2020 4669 nodejs>
<dsa 2020 4671 vlc>
<dsa 2020 4672 trafficserver>
</table>


<h2>删除的软件包</h2>

<p>由于我们无法控制的情况，以下软件包已被删除：</p>

<table border=0>
<tr><th>软件包</th>               <th>原因</th></tr>
<correction getlive "由于 Hotmail 的更改而破损">
<correction gplaycli "由于 Google API 更改而破损">
<correction kerneloops "上游服务不再可用">
<correction lambda-align2 "[arm64 armel armhf i386 mips64el ppc64el s390x] 在非 amd64 架构上破损">
<correction libmicrodns "安全问题">
<correction libperlspeak-perl "安全问题；不再获得维护">
<correction quotecolors "与更新版本的 Thunderbird 不兼容">
<correction torbirdy "与更新版本的 Thunderbird 不兼容">
<correction ugene "Non-free; fails to build">
<correction yahoo2mbox "在过去几年处于破损状态">

</table>

<h2>Debian 安装器</h2>
<p>安装器已经更新，以配合发布时包含在稳定版本中的修正内容。</p>

<h2>链接</h2>

<p>此修订版本中有更改的软件包的完整列表：</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>当前稳定发行版：</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>拟议的稳定发行版更新：</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>稳定发行版信息（发行说明，勘误等）：</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>安全公告及信息：</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>关于 Debian</h2>

<p>Debian 项目是一个自由软件开发者组织，这些志愿者为制作完全自由免费的 Debian 操作系统\
而自愿贡献时间和精力。</p>


<h2>联系信息</h2>

<p>更多信息，请访问 Debian 主页
<a href="$(HOME)/">https://www.debian.org/</a>、发送邮件至
&lt;press@debian.org&gt; ，或联系稳定版本发布团队
&lt;debian-release@lists.debian.org&gt;。</p>


