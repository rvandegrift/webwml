# translation of bugs.po to Dutch
# Templates files for webwml modules
# Copyright (C) 2003,2004 Software in the Public Interest, Inc.
#
# Frans Pop <elendil@planet.nl>, 2007, 2008.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2011.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: bugs\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2014-09-14 10:21+0100\n"
"Last-Translator: Paul Gevers <elbrus@debian.org>\n"
"Language-Team: Dutch <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.4\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "in pakket"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "met label"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "met ernst"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "in bronpakket"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "in pakketten onderhouden door"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "ingediend door"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "eigendom van"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "met status"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "met mail van"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "meest recente bugs"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "onderwerp bevat"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "met status \"in behandeling\""

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "bugmelder bevat"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "doorgestuurd bevat"

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "eigenaar bevat"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "met pakket"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "normaal"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "oude weergave"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "onbewerkt"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "ouderdom"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Herhaal samengevoegde bugs"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Bugs in omgekeerde volgorde"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "\"In behandeling\" in omgekeerde volgorde"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Ernst in omgekeerde volgorde"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr "Geen bugs voor pakketten uit"

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr "Geen"

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "testing"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "oldstable"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "stable"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "experimental"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "unstable"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "Niet gearchiveerd"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "Gearchiveerd"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "Wel en niet gearchiveerd"
