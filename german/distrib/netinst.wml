#use wml::debian::template title="Debian über das Internet installieren" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"
# $Id$
# Original Translator: Gerfried Fuchs <alfie@debian.org> 2002-01-14
# Translator: Helge Kreutzmann <debian@helgefjell.de> 2007-11-23
# Updated: Holger Wansing <linux@wansing-online.de>, 2013 + 2015 + 2016.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2021.

<p>Diese Methode zur Installation von Debian benötigt eine
funktionierende Internetverbindung <em>während</em> der Installation. Im
Vergleich zu anderen Methoden werden weniger Daten heruntergeladen, da
der Vorgang auf Ihre Anforderungen zugeschnitten ist. Sowohl Ethernet
als auch drahtloses Netzwerk werden unterstützt. Interne ISDN-Karten werden
leider <em>nicht</em> unterstützt.</p>

<p>Es gibt drei Möglichkeiten für Installationen über Netzwerk:</p>

<toc-display />

<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">Kleine CDs oder USB-Sticks</toc-add-entry>

<p>Im folgenden finden Sie die Links zu den Image-Dateien.
   Wählen Sie dort Ihre Hardware-Architektur aus.</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>Für Details lesen Sie bitte <a href="../CD/netinst/">Netzwerkinstallation 
   von einer Minimal-CD</a></p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">Winzige CDs, spezielle USB-Sticks, usw.</toc-add-entry>

<p>Sie können eine Reihe von Image-Dateien in kleiner Größe, geeignet für 
   USB-Sticks oder ähnliche Geräte, herunterladen. Schreiben Sie diese
   auf ein Medium und beginnen Sie dann die Installation, indem Sie
   von dem Medium booten.</p>

<p>Es gibt zwischen den Architekturen einige Unterschiede für die Installation
   von verschiedenen kleinen Images.
</p>

<p>Bitte beachten Sie die <a href="$(HOME)/releases/stable/installmanual">\
   Installationsanleitung für Ihre Architektur</a>, insbesondere das Kapitel
   <q>System-Installations-Medien beschaffen</q>, für weitere Details.</p>

<p>
   Hier die Links zu den verfügbaren
   Image-Dateien (in der Datei MANIFEST finden Sie nähere Informationen):
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">Booten über Netzwerk</toc-add-entry>

<p>Sie richten einen TFTP- und einen DHCP-Server (oder alternativ zu diesem
  einen BOOTP- oder RARP-Server) ein,
  der die Installationsmedien an Maschinen in Ihrem lokalen Netzwerk ausliefert.
  Falls das BIOS Ihrer Client-Maschinen dies unterstützt, können Sie dann das
  Debian-Installationssystem über Netzwerk (mit PXE oder TFTP) booten und mit
  der Installation des Restes von Debian über das Netzwerk fortfahren.</p>

<p>Nicht alle Maschinen unterstützen das Booten über Netzwerk. Aufgrund der
   zusätzlichen Anforderungen wird diese Installationsmethode nicht für
   Neubenutzer empfohlen.</p>

<p>Bitte beachten Sie die <a href="$(HOME)/releases/stable/installmanual">\
   Installationsanleitung für Ihre Architektur</a>, insbesondere das Kapitel
   <q>Dateien vorbereiten für TFTP-Netzwerk-Boot</q>, für weitere 
   Details.</p>

<p>Hier die Links zu den Image-Dateien
   (nähere Informationen finden Sie in der Datei MANIFEST):</p>

<stable-netboot-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Sollten Sie Hardware verwenden, deren Treiber <strong>das Laden nicht-freier
Firmware erfordert</strong>, können Sie einen der
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
Tarballs mit häufig verwendeten Firmware-Archiven</a> verwenden oder ein
<strong>inoffizielles</strong> Installations-Image herunterladen, das diese
<strong>nicht-freien</strong> Firmware-Dateien enthält. Eine
Anleitung zur Verwendung der Tarballs sowie allgemeine Informationen über
das Laden von Firmware während der Installation finden Sie auch in der
<a href="../releases/stable/amd64/ch06s04">Installationsanleitung</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">inoffizielle
Installations-Images für <q>Stable</q> (inklusive Firmware)</a>
</p>
</div>
